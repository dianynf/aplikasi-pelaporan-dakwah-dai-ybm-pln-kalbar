<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kegiatan;
use App\Mualaf;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

// use Carbon\Carbon;
use Carbon\Carbon as time;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $user_id = Auth::user()->role_id;

        // card jml mualaf
        $total_mualaf = DB::table("mualafs")->sum('jml_mualaf');

        // card jml kegiatan
        $total_kegiatan = DB::table("kegiatans")->count();

        // card jml wilayah
        $total_kab = DB::table("kegiatans")->distinct()->count('id_kab');
        $total_kec = DB::table("kegiatans")->distinct()->count('id_kec');
        $total_desa = DB::table("kegiatans")->distinct()->count('id_desa');

        // year grafik
        $year = date('Y');

        // grafik kegiatan
        $janu = DB::table("kegiatans")->where('month', '=', '01')->where('year', '=', $year)->count();
        $feb = DB::table("kegiatans")->where('month', '=', '02')->where('year', '=', $year)->count();
        $mar = DB::table("kegiatans")->where('month', '=', '03')->where('year', '=', $year)->count();
        $apr = DB::table("kegiatans")->where('month', '=', '04')->where('year', '=', $year)->count();
        $mei = DB::table("kegiatans")->where('month', '=', '05')->where('year', '=', $year)->count();
        $jan = DB::table("kegiatans")->where('month', '=', '06')->where('year', '=', $year)->count();
        $jul = DB::table("kegiatans")->where('month', '=', '07')->where('year', '=', $year)->count();
        $ags = DB::table("kegiatans")->where('month', '=', '08')->where('year', '=', $year)->count();
        $sep = DB::table("kegiatans")->where('month', '=', '09')->where('year', '=', $year)->count();
        $okt = DB::table("kegiatans")->where('month', '=', '10')->where('year', '=', $year)->count();
        $nov = DB::table("kegiatans")->where('month', '=', '11')->where('year', '=', $year)->count();
        $des = DB::table("kegiatans")->where('month', '=', '12')->where('year', '=', $year)->count();

        // grafik mualaf
        $janum = DB::table("mualafs")->where('month', '=', '01')->where('year', '=', $year)->sum('jml_mualaf');
        $febm = DB::table("mualafs")->where('month', '=', '02')->where('year', '=', $year)->sum('jml_mualaf');
        $marm = DB::table("mualafs")->where('month', '=', '03')->where('year', '=', $year)->sum('jml_mualaf');
        $aprm = DB::table("mualafs")->where('month', '=', '04')->where('year', '=', $year)->sum('jml_mualaf');
        $meim = DB::table("mualafs")->where('month', '=', '05')->where('year', '=', $year)->sum('jml_mualaf');
        $janm = DB::table("mualafs")->where('month', '=', '06')->where('year', '=', $year)->sum('jml_mualaf');
        $julm = DB::table("mualafs")->where('month', '=', '07')->where('year', '=', $year)->sum('jml_mualaf');
        $agsm = DB::table("mualafs")->where('month', '=', '08')->where('year', '=', $year)->sum('jml_mualaf');
        $sepm = DB::table("mualafs")->where('month', '=', '09')->where('year', '=', $year)->sum('jml_mualaf');
        $oktm = DB::table("mualafs")->where('month', '=', '10')->where('year', '=', $year)->sum('jml_mualaf');
        $novm = DB::table("mualafs")->where('month', '=', '11')->where('year', '=', $year)->sum('jml_mualaf');
        $desm = DB::table("mualafs")->where('month', '=', '12')->where('year', '=', $year)->sum('jml_mualaf');

        // card jml dai
        $total_dai =
            DB::table('users')
            ->join('roles', 'roles.id', '=', 'users.role_id')
            ->where('users.role_id', '=', 2)
            ->get()->count('role_id');

        return view('pages.dashboard.index', [
            'total_mualaf' => $total_mualaf,
            'total_kegiatan' => $total_kegiatan,
            'total_dai' => $total_dai,
            'total_kab' => $total_kab,
            'total_kec' => $total_kec,
            'total_desa' => $total_desa,
            'janu' => $janu,
            'feb' => $feb,
            'mar' => $mar,
            'apr' => $apr,
            'mei' => $mei,
            'jan' => $jan,
            'jul' => $jul,
            'ags' => $ags,
            'sep' => $sep,
            'okt' => $okt,
            'nov' => $nov,
            'des' => $des,

            'janum' => $janum,
            'febm' => $febm,
            'marm' => $marm,
            'aprm' => $aprm,
            'meim' => $meim,
            'janm' => $janm,
            'julm' => $julm,
            'agsm' => $agsm,
            'sepm' => $sepm,
            'oktm' => $oktm,
            'novm' => $novm,
            'desm' => $desm,
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function edit(Request $request)
    {
        return view('pages.profile.edit', [
            'users' => $request->user()
        ]);
    }

    public function update(Request $request)
    {
        $image = $request->file('image');
        // dd($image);
        $this->validate($request, [
            'image' => 'file|max:2000'
        ]);

        if ($image) {
            $request->user()->update([
                'username' => $request->username,
                'alamat' => $request->alamat,
                'nohp' => $request->nohp,
                'email' => $request->email,
                'image' => $image->getClientOriginalName()
            ]);
            $image->storeAs('public/fotoselfi', $image->getClientOriginalName());
        } else {
            $request->user()->update([
                'username' => $request->username,
                'alamat' => $request->alamat,
                'nohp' => $request->nohp,
                'email' => $request->email,
            ]);
        }

        session()->flash('sukses', 'Profile berhasil di Ubah');
        return redirect()->route('profile.edit');
    }
}

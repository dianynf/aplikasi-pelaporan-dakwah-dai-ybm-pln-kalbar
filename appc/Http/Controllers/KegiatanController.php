<?php

namespace App\Http\Controllers;

use App\Kegiatan;
use App\KegiatanDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Reg_districts;
use App\Reg_regencies;
use App\Reg_villages;
use App\User;
use Illuminate\Support\Facades\DB;

use App\Exports\KegiatansExport;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpKernel\Kernel;

class KegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // CMS
    public function index()
    {
        $user_id = Auth::user()->role_id;
        if ($user_id == 1) {

            $items = Kegiatan::with([
                'user', 'desa', 'kab', 'kec'
            ])->get();

            return view('pages.cms.kegiatan.index', [
                'items' => $items
            ]);
        } elseif ($user_id == 2) {
            $items = Kegiatan::with([
                'user', 'desa', 'kab', 'kec'
            ])->where('users_id', '=', Auth::user()->id)
                ->get();
            return view('pages.cms.kegiatan.index', [
                'items' => $items
            ]);
        }
    }

    public function show($id)
    {
        $item = Kegiatan::with([
            'fotos', 'user', 'kab', 'kec', 'desa'
        ])->findOrFail($id);
        // dd($item->fotos);
        return view('pages.cms.kegiatan.detail', [
            'item' => $item
        ]);
    }

    public function edit(Kegiatan $kegiatan)
    {
        $user_id = Auth::user()->role_id;
        if ($user_id == 1) {
            $departmentData['data'] = Reg_regencies::orderby("name", "asc")
                ->select('id', 'name')
                ->get();
            $user = User::all();
            $fotos = KegiatanDetail::all();
            $kab = Reg_regencies::all();
            $kec = Reg_districts::all();
            $desa = Reg_villages::all();
            return view('pages.cms.kegiatan.edit', compact('departmentData', 'fotos', 'kegiatan', 'kab', 'desa', 'kec', 'user'));
        } else {
            return abort(404);
        }
    }

    public function update(Request $request, Kegiatan $kegiatan)
    {
        // $kegiatan = Kegiatan::findOrFail($id);
        // dd($kegiatan->id);
        $this->validate($request, [
            'tgl' => 'required',
            'jml_kajian' => 'required|numeric',
            'jml_jamaah' => 'required|numeric',
            'alamat' => 'required',
            'deskripsi' => 'required',
            'id_kab' => 'required',
            'id_kec' => 'required',
            'id_desa' => 'required',
        ]);

        $kegiatan->update([
            'tgl' => $request->tgl,
            'jml_kajian' => $request->jml_kajian,
            'jml_jamaah' => $request->jml_jamaah,
            'deskripsi' => $request->deskripsi,
            'alamat' => $request->alamat,
            'id_kab' => $request->id_kab,
            'id_kec' => $request->id_kec,
            'id_desa' => $request->id_desa,
            'sasaran' => $request->sasaran,
            'evaluasi' => $request->evaluasi
        ]);


        if ($request->file("foto") == null) {
            session()->flash('sukses', 'Data kegiatan berhasil edit');
            return redirect(route('kegiatans.index'));
        }

        $files = [];
        foreach ($request->file("foto") as $file) {
            $images = KegiatanDetail::where("kegiatans_id", $kegiatan->id)->get();
            foreach ($images as $image) {
                if (File::exists("storage/dokumentasi/" . $image->foto)) {
                    File::delete("storage/dokumentasi/" . $image->foto);
                }
            }

            $kegiatan->fotos()->delete();
            $files[] = [
                'kegiatans_id' => $kegiatan->id,
                'foto' => $file->getClientOriginalName()
            ];
            $file->storeAs('public/dokumentasi', $file->getClientOriginalName());
        }
        KegiatanDetail::insert($files);

        session()->flash('sukses', 'Data kegiatan berhasil edit');
        return redirect(route('kegiatans.index'));
    }

    public function destroy(Kegiatan $kegiatan)
    {
        $user_id = Auth::user()->role_id;
        if ($user_id == 1) {
            $images = KegiatanDetail::where("kegiatans_id", $kegiatan->id)->get();
            foreach ($images as $image) {
                if (File::exists("storage/dokumentasi/" . $image->foto)) {
                    File::delete("storage/dokumentasi/" . $image->foto);
                }
            }
            $kegiatan->delete();
            $kegiatan->fotos()->delete();
            session()->flash('sukses', 'Data berhasil dihapus');
            return redirect()->route('kegiatans.index');
        } else {
            return abort(404);
        }
    }

    public function export_excel()
    {
        return Excel::download(new KegiatansExport, 'Kegiatan Dai 2021.xlsx');
    }

    // LMS

    public function input()
    {
        $departmentData['data'] = Reg_regencies::orderby("name", "asc")
            ->select('id', 'name')
            ->get();

        $daiData['data'] = User::orderby("username", "asc")
            ->select('id', 'username')
            ->get();

        return view('pages.kegiatan.index', compact('departmentData', 'daiData'));
    }

    public function getDistricts($departmentid = 0)
    {
        $empData['data'] = Reg_districts::orderby("name", "asc")
            ->select('id', 'name')
            ->where('regency_id', $departmentid)
            ->get();

        return response()->json($empData);
    }

    public function getVillages($distid = 0)
    {
        $distData['data'] = Reg_villages::orderby("name", "asc")
            ->select('id', 'name')
            ->where('district_id', $distid)
            ->get();

        return response()->json($distData);
    }

    public function store(Request $request)
    {
        $expiry = explode("/", $request->tgl);

        $date = $expiry[0];
        $month = $expiry[1];
        $year = $expiry[2];

        if (Auth::user()->role_id == 1) {
            $lastid = Kegiatan::create([
                'users_id' => $request->users_id,
                'tgl' => $request->tgl,
                'jml_kajian' => $request->jml_kajian,
                'jml_jamaah' => $request->jml_jamaah,
                'deskripsi' => $request->deskripsi,
                'alamat' => $request->alamat,
                'id_kab' => $request->id_kab,
                'id_kec' => $request->id_kec,
                'id_desa' => $request->id_desa,
                'date' => $date,
                'month' => $month,
                'year' => $year,
                'sasaran' => $request->sasaran,
                'evaluasi' => $request->evaluasi,
            ])->id;
        } else {
            $lastid = Kegiatan::create([
                'users_id' => Auth::user()->id,
                'tgl' => $request->tgl,
                'jml_kajian' => $request->jml_kajian,
                'jml_jamaah' => $request->jml_jamaah,
                'deskripsi' => $request->deskripsi,
                'alamat' => $request->alamat,
                'id_kab' => $request->id_kab,
                'id_kec' => $request->id_kec,
                'id_desa' => $request->id_desa,
                'date' => $date,
                'month' => $month,
                'year' => $year,
                'sasaran' => $request->sasaran,
                'evaluasi' => $request->evaluasi,
            ])->id;
        }

        $files = [];
        foreach ($request->file('foto') as $file) {
            $files[] = [
                'kegiatans_id' => $lastid,
                'foto' => $file->getClientOriginalName()
            ];
            $file->storeAs('public/dokumentasi', $file->getClientOriginalName());
        }
        KegiatanDetail::insert($files);

        session()->flash('sukses', 'Data berhasil di Input');
        return redirect('/kegiatans');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function profiles()
    {
        return view('pages.menu.profiles');
    }

    public function berita()
    {
        return view('pages.menu.berita');
    }

    public function gambar()
    {
        return view('pages.menu.gambar');
    }

    public function kontak()
    {
        return view('pages.menu.kontak');
    }

    public function store(Request $request)
    {
        $request->validate([
            'email' => ['required', 'string', 'max:100', 'email',],
            'deskripsi' => ['required', 'string', 'max:255'],
            'subjek' => ['string', 'max:200'],
            'nama' => ['max:50'],
        ]);
        DB::table('kontaks')->insert([
            'email' => $request->email,
            'deskripsi' => $request->deskripsi,
            'subjek' => $request->subjek ?? null,
            'nama' => $request->nama ?? null
        ]);
        session()->flash('kontak', 'Pesan Anda telah dikirim. Terima kasih');
        return back()->withInput();
    }

    public function ustyanto()
    {
        return view('pages.menu.menu-berita.ustyanto');
    }

    public function beting_to_bening()
    {
        return view('pages.menu.menu-berita.beting_to_bening');
    }

    public function training_imam_dan_khatib()
    {
        return view('pages.menu.menu-berita.training_imam_dan_khatib');
    }
}

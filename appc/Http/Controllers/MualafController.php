<?php

namespace App\Http\Controllers;

use App\Mualaf;
use App\MualafDetails;
use App\Http\Controllers\Controller;
use App\MualafDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Reg_regencies;
use App\Reg_districts;
use App\Reg_villages;
use App\User;
use Illuminate\Support\Facades\DB;

use App\Exports\MualafsExport;
use Maatwebsite\Excel\Facades\Excel;


class MualafController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // bagian CMS
    public function index()
    {
        $user_id = Auth::user()->role_id;
        if ($user_id == 1) {
            $items = Mualaf::with([
                'user'
            ])->get();
            return view('pages.cms.mualaf.index', [
                'items' => $items
            ]);
        } elseif ($user_id == 2) {
            $items = Mualaf::with([
                'user'
            ])->where('users_id', '=', Auth::user()->id)
                ->get();
            return view('pages.cms.mualaf.index', [
                'items' => $items
            ]);
        }
    }

    public function show(Mualaf $mualaf)
    {
        $user = User::all();
        $mualafs = MualafDetail::all();

        return view('pages.cms.mualaf.detail', [
            'mualaf' => $mualaf,
            'mualafs' => $mualafs,
            'user' => $user
        ]);
    }

    public function edit(Mualaf $mualaf)
    {
        $user_id = Auth::user()->role_id;
        if ($user_id == 1) {
            $user = User::all();
            $mualafs = MualafDetail::all();

            return view('pages.cms.mualaf.edit', [
                'mualaf' => $mualaf,
                'user' => $user,
                'mualafs' => $mualafs
            ]);
        } else {
            return abort(404);
        }
    }

    public function update(Request $request, Mualaf $mualaf)
    {
        $this->validate($request, [
            'tgl' => 'required',
            'jml_mualaf' => 'required|numeric'
        ]);

        $mualaf->update([
            'tgl' => $request->tgl,
            'jml_mualaf' => $request->jml_mualaf,
        ]);

        $this->validate($request, [
            'foto.*' => 'mimes:jpeg,jpg,png|max:2000',
            'nama' => 'required',
            'alamat' => 'required',
            'deskripsi' => 'required'
        ]);

        if ($request->file("foto") == null) {
            if (count($request->id) > 0) {
                foreach ($request->id as $item => $v) {
                    $datad = array(
                        'nama' => $request->nama[$item],
                        'alamat' => $request->alamat[$item],
                        'deskripsi' => $request->deskripsi[$item],
                    );
                    $dbazar = MualafDetail::where('id', $request->id[$item])->first();
                    $dbazar->update($datad);
                }
                session()->flash('sukses', 'Data Mualaf berhasil edit');
                return redirect(route('mualafs.index'));
            }
        }

        if (count($request->file('foto')) > 0) {
            foreach ($request->file('foto') as $item => $v) {
                $datad = array(
                    'nama' => $request->nama[$item],
                    'alamat' => $request->alamat[$item],
                    'deskripsi' => $request->deskripsi[$item],
                    'foto' => $v->getClientOriginalName()
                );
                $v->storeAs('public/mualaf', $v->getClientOriginalName());
                $dbazar = MualafDetail::where('id', $request->id[$item])->first();
                $dbazar->update($datad);
            }
            session()->flash('sukses', 'Data Mualaf berhasil edit');
            return redirect(route('mualafs.index'));
        }

        // $images = MualafDetail::where("foto", $request->foto)->get();
        // dd($images);
        // if (count($request->file('foto')) > 0) {
        //     foreach ($request->file('foto') as $item => $v) {
        //         dd($images);
        //         foreach ($images as $image) {
        //             if (File::exists("storage/mualaf/" . $image->foto)) {
        //                 File::delete("storage/mualaf/" . $image->foto);
        //             }
        //             // dd($image->foto);
        //             $datad = array(
        //                 'nama' => $request->nama[$item],
        //                 'alamat' => $request->alamat[$item],
        //                 'deskripsi' => $request->deskripsi[$item],
        //                 'foto' => $v->getClientOriginalName()
        //             );
        //             $v->storeAs('public/mualaf', $v->getClientOriginalName());
        //             $dbazar = MualafDetail::where('id', $request->id[$item])->first();
        //             $dbazar->update($datad);
        //         }
        //     }
        //     session()->flash('sukses', 'Data Mualaf berhasil edit');
        //     return redirect(route('mualafs.index'));
        // }
    }

    public function destroy(Mualaf $mualaf)
    {
        $user_id = Auth::user()->role_id;
        if ($user_id == 1) {
            $images = MualafDetail::where("mualafs_id", $mualaf->id)->get();
            foreach ($images as $image) {
                if (File::exists("storage/mualaf/" . $image->foto)) {
                    File::delete("storage/mualaf/" . $image->foto);
                }
            }
            $mualaf->delete();
            $mualaf->fotos()->delete();
            session()->flash('sukses', 'Data berhasil dihapus');
            return redirect()->route('mualafs.index');
        } else {
            return abort(404);
        }
    }

    public function export_excel()
    {
        return Excel::download(new MualafsExport, 'Data Mualaf 2021.xlsx');
    }

    // bagian LMS
    public function input(Request $request)
    {
        $daiData['data'] = User::orderby("username", "asc")
            ->select('id', 'username')
            ->get();

        return view('pages.mualaf.index', compact('daiData'));
    }

    public function create(Request $request)
    {
        $expiry = explode("/", $request->tgl);

        $date = $expiry[0];
        $month = $expiry[1];
        $year = $expiry[2];

        if (Auth::user()->role_id == 1) {
            $lastid = Mualaf::create([
                'users_id' => $request->users_id,
                'jml_mualaf' => $request->jml_mualaf,
                'tgl' => $request->tgl,
                'date' => $date,
                'month' => $month,
                'year' => $year
            ])->id;
        } else {
            $lastid = Mualaf::create([
                'users_id' => Auth::user()->id,
                'jml_mualaf' => $request->jml_mualaf,
                'tgl' => $request->tgl,
                'date' => $date,
                'month' => $month,
                'year' => $year
            ])->id;
        }

        $this->validate($request, [
            'foto' => 'required',
            'foto.*' => 'mimes:jpeg,jpg,png|max:2000',
            'nama' => 'required',
            'alamat' => 'required',
            'deskripsi' => 'required'
        ]);

        if (count($request->file('foto')) > 0) {
            foreach ($request->file('foto') as $item => $v) {
                $datad = array(
                    'mualafs_id' => $lastid,
                    'nama' => $request->nama[$item],
                    'alamat' => $request->alamat[$item],
                    'deskripsi' => $request->deskripsi[$item],
                    'foto' => $v->getClientOriginalName()
                );
                $v->storeAs('public/mualaf', $v->getClientOriginalName());
                MualafDetail::insert($datad);
            }
        }

        session()->flash('sukses', 'Data berhasil di Input');
        return redirect('/mualafs');
    }
}

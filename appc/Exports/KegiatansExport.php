<?php

namespace App\Exports;

use App\Kegiatan;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;


class KegiatansExport implements FromView, WithDrawings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function view(): View
    {
        $data['export_excel'] = Kegiatan::all();
        return view('pages.cms.kegiatan.export_excel', $data);
    }

    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('This is my logo');
        $drawing->setPath(public_path('/fronten/assets/img/icons.png'));
        $drawing->setHeight(100);
        $drawing->setWidth(100);
        $drawing->setCoordinates('B2');

        return $drawing;
    }
}

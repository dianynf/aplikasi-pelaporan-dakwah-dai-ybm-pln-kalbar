<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MualafDetail extends Model
{
    protected $fillable = [
        'mualafs_id', 'foto', 'nama', 'alamat', 'deskripsi'
    ];
    protected $hidden = [];

    public function mualaf()
    {
        return $this->belongsTo(Mualaf::class, 'mualafs_id', 'id');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMualafsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mualafs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('jml_mualaf');
            $table->text('deskripsi')->nullable();
            $table->string('tgl');
            $table->string('alamat')->nullable();
            $table->bigInteger('id_kab');
            $table->bigInteger('id_kec');
            $table->bigInteger('id_desa');
            $table->bigInteger('users_id');
            $table->bigInteger('users_id');
            $table->bigInteger('year');
            $table->bigInteger('date');
            $table->string('month')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mualafs');
    }
}

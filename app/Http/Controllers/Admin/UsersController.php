<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
// use App\Http\Requests\Admin\UsersRequest;
// use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Reg_districts;
use App\Reg_regencies;
use App\Reg_villages;
use App\User;
use App\Role;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $items = User::with([
            'role'
        ])->get();
        return view('pages.admin.users.index', [
            'items' => $items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return view('pages.admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'nohp' => ['required', 'min:10', 'max:15'],
        ]);
        User::create([
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'nohp' => $request->nohp,
            'email_verified_at' => $request->date,
            'role_id' => 2,
            'image' => 'defaultUserDaiYbmPln.jpg',

        ]);
        session()->flash('sukses', 'Data user berhasil ditambahkan');
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $roles = Role::all();
        $desa = Reg_villages::all();
        return view('pages.admin.users.detail', [
            'user' => $user,
            'roles' => $roles,
            'desa' => $desa
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(User $user)
    {
        $roles = Role::all();
        return view('pages.admin.users.edit', [
            'user' => $user,
            'roles' => $roles
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, User $user)
    {
        $user->role_id = $request->role_id;
        if ($request->role_id == "1") {
            $user->roles = "ADMIN";
        } else if ($request->role_id == "2") {
            $user->roles = "USER";
        }
        $user->save();

        session()->flash('sukses', 'Data user berhasil edit');
        return redirect()->route('users.index');
    }

    public function destroy(User $user)
    {
        $user->delete();
        session()->flash('sukses', 'Data User Berhasil Hapus');
        return redirect()->route('users.index');
    }

    // modul dai

    public function dai()
    {
        $items = User::with([
            'role'
        ])->where('role_id', '=', '2')
            ->get();
        return view('pages.admin.users.dai.dai', [
            'items' => $items
        ]);
    }

    public function create_dai()
    {
        return view('pages.admin.users.dai.create');
    }

    public function store_dai(Request $request)
    {
        $request->validate([
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'nohp' => ['required', 'min:10', 'max:15'],
        ]);
        User::create([
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'nohp' => $request->nohp,
            'email_verified_at' => $request->date,
            'role_id' => 2,
            'image' => 'defaultUserDaiYbmPln.jpg',

        ]);
        session()->flash('sukses', 'Data Dai berhasil ditambahkan');
        return redirect()->route('dai.index');
    }

    public function ubah(User $user)
    {
        $roles = Role::all();
        $desa = Reg_villages::all();
        return view('pages.admin.users.dai.ubah', [
            'user' => $user,
            'roles' => $roles,
            'desa' => $desa
        ]);
    }

    public function simpan(Request $request, User $user)
    {
        $user->role_id = $request->role_id;
        $user->save();

        if ($user->save()) {
            $request->session()->flash('success', $user->name . 'User has been updated');
        } else {
            $request->session()->flash('error', 'User has been error updated');
        }

        $user->desa()->attach($request->desa);
        session()->flash('sukses', 'Data dai berhasil edit');
        return redirect()->route('dai.index');
    }

    public function detail(User $user)
    {
        $roles = Role::all();
        // $kab = Reg_regencies::all();
        // $kec = Reg_districts::all();
        // $desa = Reg_villages::all();
        $desa = Reg_villages::get();
        return view('pages.admin.users.dai.detail', [
            'user' => $user,
            'roles' => $roles,
            // 'kab' => $kab,
            'desa' => $desa,
            // 'kec' => $kec,
        ]);
    }

    public function delete(User $user)
    {
        $user->delete();
        session()->flash('sukses', 'Data Dai Berhasil Hapus');
        return redirect()->route('dai.index');
    }
}

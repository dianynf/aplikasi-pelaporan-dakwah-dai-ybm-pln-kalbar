<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PesanRequest;
use App\Kontak;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PesanController extends Controller
{
    public function index()
    {
        $pesan = Kontak::all();

        return view('pages.admin.pesan.index', [
            'pesan' => $pesan
        ]);
    }

    public function edit($id)
    {
        $kontak = Kontak::findOrFail($id);
        return view('pages.admin.pesan.edit', [
            'kontak' => $kontak
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $item = Kontak::findOrFail($id);

        $item->update($data);

        session()->flash('sukses', 'Pesan berhasil ubah');
        return redirect()->route('pesan.index');
    }

    public function destroy($id)
    {
        $kontak = Kontak::findorFail($id);
        $kontak->delete();
        session()->flash('sukses', 'Pesan berhasil di Hapus');
        return redirect(route('pesan.index'));
    }
}

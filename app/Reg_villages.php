<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reg_villages extends Model
{
    protected $fillable = [
        'district_id', 'name'
    ];

    public function mualafs()
    {
        return $this->hasMany(Mualaf::class, 'id_desa', 'id');
    }

    public function kegiatans()
    {
        return $this->hasMany(Kegiatan::class, 'id_desa', 'id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reg_regencies extends Model
{
    protected $fillable = [
        'province_id', 'name'
    ];

    public function mualafs()
    {
        return $this->hasMany(Mualaf::class, 'id_kab', 'id');
    }

    public function kegiatans()
    {
        return $this->hasMany(Kegiatan::class, 'id_kab', 'id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'id_kab', 'id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KegiatanDetail extends Model
{
    protected $fillable = [
        'kegiatans_id', 'foto'
    ];
    // protected $casts = [
    //     'foto' => array()
    // ];
    // protected $hidden = [];

    public function kegiatan()
    {
        return $this->belongsTo(Kegiatan::class, 'kegiatans_id', 'id');
    }
}

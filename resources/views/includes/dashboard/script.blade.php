<script src="{{ url('backend/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ url('backend/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ url('backend/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
<script src="{{ url('backend/js/sb-admin-2.min.js') }}"></script>

<script src="{{ url('backend/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('backend/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ url('backend/js/demo/datatables-demo.js') }}"></script>

<script src="{{ url('backend/vendor/chart.js/Chart.min.js') }}"></script>

<script src="{{ url('backend/js/demo/chart-area-demo.js') }}"></script>
<script src="{{ url('backend/js/demo/chart-pie-demo.js') }}"></script>
<script src="{{ url('backend/js/demo/chart-bar-demo.js') }}"></script>
<script src="{{ url('backend/js/demo/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

<script>
    $('select').selectpicker();
</script>

<script>
    $(function() {
        $('#only-number').on('keydown', '#nohp', function(e) {
            -1 !== $
                .inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || /65|67|86|88/
                .test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey) ||
                35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e
                    .keyCode) &&
                (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
        });
    });

    $(document).ready(function() {
        $("#show_hide_password a").on('click', function(event) {
            event.preventDefault();
            if ($('#show_hide_password input').attr("type") == "text") {
                $('#show_hide_password input').attr('type', 'password');
                $('#show_hide_password i').addClass("fa-eye-slash");
                $('#show_hide_password i').removeClass("fa-eye");
            } else if ($('#show_hide_password input').attr("type") == "password") {
                $('#show_hide_password input').attr('type', 'text');
                $('#show_hide_password i').removeClass("fa-eye-slash");
                $('#show_hide_password i').addClass("fa-eye");
            }
        });
    });

    $(document).ready(function() {
        $("#show_hide_passwordd a").on('click', function(event) {
            event.preventDefault();
            if ($('#show_hide_passwordd input').attr("type") == "text") {
                $('#show_hide_passwordd input').attr('type', 'password');
                $('#show_hide_passwordd i').addClass("fa-eye-slash");
                $('#show_hide_passwordd i').removeClass("fa-eye");
            } else if ($('#show_hide_passwordd input').attr("type") == "password") {
                $('#show_hide_passwordd input').attr('type', 'text');
                $('#show_hide_passwordd i').removeClass("fa-eye-slash");
                $('#show_hide_passwordd i').addClass("fa-eye");
            }
        });
    });

    $(document).ready(function() {
        $("#show_hide_password_login a").on('click', function(event) {
            event.preventDefault();
            if ($('#show_hide_password_login input').attr("type") == "text") {
                $('#show_hide_password_login input').attr('type', 'password');
                $('#show_hide_password_login i').addClass("fa-eye-slash");
                $('#show_hide_password_login i').removeClass("fa-eye");
            } else if ($('#show_hide_password_login input').attr("type") == "password") {
                $('#show_hide_password_login input').attr('type', 'text');
                $('#show_hide_password_login i').removeClass("fa-eye-slash");
                $('#show_hide_password_login i').addClass("fa-eye");
            }
        });
    });

</script>

<script>
    $(document).ready(function() {
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true,
        });
    });

</script>

<script>
    var ctx = document.getElementById("kegiatanChart");
    var myBarChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September",
                "Oktober", "November", "Desember"
            ],
            datasets: [{
                label: " Jumlah Kegiatan Da'i",
                backgroundColor: "#4e73df",
                hoverBackgroundColor: "#2e59d9",
                borderColor: "#4e73df",
                data: [
                    {{ $janu }}, {{ $feb }}, {{ $mar }},
                    {{ $apr }}, {{ $mei }}, {{ $jan }},
                    {{ $jul }}, {{ $ags }}, {{ $sep }},
                    {{ $okt }}, {{ $nov }}, {{ $des }}
                ],
            }],
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'month'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 12
                    },
                    maxBarThickness: 25,
                }],
                yAxes: [{
                    ticks: {
                        min: 0,
                        max: 500,
                        maxTicksLimit: 9,
                        padding: 10,
                        // Include a dollar sign in the ticks
                        callback: function(value, index, values) {
                            return number_format(value);
                        }
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: false
            },
            tooltips: {
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                caretPadding: 10,
                callbacks: {
                    label: function(tooltipItem, chart) {
                        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                        return number_format(tooltipItem.yLabel) + datasetLabel;
                    }
                }
            },
        }
    });

    var ctx = document.getElementById("myMualafs");
    var myMualafs = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September",
                "Oktober", "November", "Desember"
            ],
            datasets: [{
                label: " Jumlah Mualaf",
                backgroundColor: "#1cc88a",
                hoverBackgroundColor: "#1cc88a",
                borderColor: "#4e73df",
                data: [
                    {{ $janum }}, {{ $febm }}, {{ $marm }},
                    {{ $aprm }}, {{ $meim }}, {{ $janm }},
                    {{ $julm }}, {{ $agsm }}, {{ $sepm }},
                    {{ $oktm }}, {{ $novm }}, {{ $desm }}
                ],
            }],
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'month'
                    },
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 12
                    },
                    maxBarThickness: 25,
                }],
                yAxes: [{
                    ticks: {
                        min: 0,
                        max: 500,
                        maxTicksLimit: 9,
                        padding: 10,
                        // Include a dollar sign in the ticks
                        callback: function(value, index, values) {
                            return number_format(value);
                        }
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            },
            legend: {
                display: false
            },
            tooltips: {
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                caretPadding: 10,
                callbacks: {
                    label: function(tooltipItem, chart) {
                        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                        return number_format(tooltipItem.yLabel) + datasetLabel;
                    }
                }
            },
        }
    });

</script>

<footer id="footer" class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 ml-md-auto footer-info">
                    <a href="#" class="logo d-flex align-items-center">
                        <img class="img-fluid" src="{{ asset('fronten/assets/img/logo/logo-footer.png') }}"
                            alt="YBM PLN" style="max-height: 80px;">
                    </a>
                    <h3 class="mt-5">Yayasan Baitul Maal PLN</h3>
                    <p class="mt-2"><i class="icofont-google-map"></i> Alamat: Jl. Adi Sucipto No.4, Sungai Raya,
                        Kec. Sungai Raya, <br> Kabupaten Kubu Raya, Kalimantan Barat 78234</p>
                    <p class="mt-4"><i class="icofont-envelope"></i> Email: ybmpln@gmail.com</p>
                    <p class="mt-1"><i class="icofont-phone"></i> Nomor Telpon: +62 853-9364-1141</p>
                </div>

                <div class="col-lg-5 ml-md-auto footer-links">
                    <h4>OPERASIONAL KANTOR</h4>
                    <ul>
                        <li><i class="fa fa-clock-o"></i> Senin - Jumat : 8.00 am - 16.00 pm</li>
                        <li><i class="fa fa-clock-o"></i> Sabtu - Ahad :<a class="bg-white btn-tutup">Tutup</a></li>
                    </ul>
                    <h4 class="mt-5">Ikuti Kami</h4>
                    <div class="social-links mt-2">
                        <a href="https://www.twitter.com/ybmpln/" class="twitter"><i class="icofont-twitter"></i></a>
                        <a href="https://www.facebook.com/ybmpln/" class="facebook"><i class="icofont-facebook"></i></a>
                        <a href="https://www.instagram.com/ybmpln/" class="instagram"><i
                                class="icofont-instagram"></i></a>
                        <a href="https://www.youtube.com/channel/UCD8sZAllQgkNvXf43sHS1tQ" class="youtube"><i
                                class="icofont-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="copyright">
            &copy; Copyright <strong><span>YBM PLN KAL-BAR</span></strong>
        </div>
    </div>
</footer>

<script src="{{ url('backend/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ url('backend/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ url('backend/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
<script src="{{ url('backend/js/sb-admin-2.min.js') }}"></script>

<script src="{{ url('backend/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('backend/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ url('backend/js/demo/datatables-demo.js') }}"></script>

<script src="{{ url('backend/vendor/chart.js/Chart.min.js') }}"></script>

<script src="{{ url('backend/js/demo/chart-area-demo.js') }}"></script>
<script src="{{ url('backend/js/demo/chart-pie-demo.js') }}"></script>
<script src="{{ url('backend/js/demo/chart-bar-demo.js') }}"></script>
<script src="{{ url('backend/js/demo/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">

        $('.show_confirm').click(function(event) {
            var form =  $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Apakah Anda Yaqin?`,
                text: "Data yang sudah dihapus tidak dapat dikembalikan.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    form.submit();
                }
            });
        });

    </script>

<script>
    $('select').selectpicker();

</script>

<script>
    $(function() {
        $('#only-number').on('keydown', '#nohp', function(e) {
            -1 !== $
                .inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || /65|67|86|88/
                .test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey) ||
                35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e
                    .keyCode) &&
                (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
        });
    });

    $(document).ready(function() {
        $("#show_hide_password a").on('click', function(event) {
            event.preventDefault();
            if ($('#show_hide_password input').attr("type") == "text") {
                $('#show_hide_password input').attr('type', 'password');
                $('#show_hide_password i').addClass("fa-eye-slash");
                $('#show_hide_password i').removeClass("fa-eye");
            } else if ($('#show_hide_password input').attr("type") == "password") {
                $('#show_hide_password input').attr('type', 'text');
                $('#show_hide_password i').removeClass("fa-eye-slash");
                $('#show_hide_password i').addClass("fa-eye");
            }
        });
    });

    $(document).ready(function() {
        $("#show_hide_passwordd a").on('click', function(event) {
            event.preventDefault();
            if ($('#show_hide_passwordd input').attr("type") == "text") {
                $('#show_hide_passwordd input').attr('type', 'password');
                $('#show_hide_passwordd i').addClass("fa-eye-slash");
                $('#show_hide_passwordd i').removeClass("fa-eye");
            } else if ($('#show_hide_passwordd input').attr("type") == "password") {
                $('#show_hide_passwordd input').attr('type', 'text');
                $('#show_hide_passwordd i').removeClass("fa-eye-slash");
                $('#show_hide_passwordd i').addClass("fa-eye");
            }
        });
    });

    $(document).ready(function() {
        $("#show_hide_password_login a").on('click', function(event) {
            event.preventDefault();
            if ($('#show_hide_password_login input').attr("type") == "text") {
                $('#show_hide_password_login input').attr('type', 'password');
                $('#show_hide_password_login i').addClass("fa-eye-slash");
                $('#show_hide_password_login i').removeClass("fa-eye");
            } else if ($('#show_hide_password_login input').attr("type") == "password") {
                $('#show_hide_password_login input').attr('type', 'text');
                $('#show_hide_password_login i').removeClass("fa-eye-slash");
                $('#show_hide_password_login i').addClass("fa-eye");
            }
        });
    });

</script>

<script>
    $(document).ready(function() {
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true,
        });
    });

</script>

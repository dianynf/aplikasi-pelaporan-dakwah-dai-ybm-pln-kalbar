<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; YBM PLN Kalimantan Barat <?= date('Y') ?></span>
    </div>
  </div>
</footer>

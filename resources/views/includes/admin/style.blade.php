<!-- Favicons -->
<link href="{{ url('fronten/assets/img/favicon.ico') }}" rel="icon">
<link href="{{ url('fronten/assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">
<!-- Custom fonts for this template-->
<link href="{{ url('backend/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
<link
    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

<!-- Custom styles for this template-->
<link href="{{ url('backend/css/sb-admin-2.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ url('backend/css/style.css') }}">
<link rel="stylesheet" href="{{ url('backend/vendor/datatables/dataTables.bootstrap4.min.css') }}">
<link href="{{ url('fronten/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}"
    rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />

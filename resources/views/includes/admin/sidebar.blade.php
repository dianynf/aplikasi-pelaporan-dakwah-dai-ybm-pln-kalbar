<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('home') }}">
        <div class="sidebar-brand-icon">
            @if (Auth::user()->role_id == 1)
                    Admin
            @else
                Dai
            @endif
        </div>
        <div class="sidebar-brand-text mx-1">
            YBM PLN
        </div>
    </a>

    <hr class="sidebar-divider my-0">
    <li class="nav-item">
        <a class="nav-link" href="{{ route('dashboard.index') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
    @if (Auth::user()->role_id == 1)
        <hr class="sidebar-divider">

        <li class="nav-item">
            <a class="nav-link" href="{{ route('kegiatans.index') }}">
                <i class="fas fa-fw fa-chart-area"></i>
                <span>Data Kegiatan Dai</span></a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ route('mualafs.index') }}">
                <i class="fas fa-fw fa-chart-area"></i>
                <span>Data Mualaf</span></a>
        </li>

        <hr class="sidebar-divider">

        <div class="sidebar-heading">
            Admin
        </div>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('pesan.index') }}">
                <i class="fas fa-fw fa-envelope"></i>
                <span>Pesan Masuk</span></a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ route('dai.index') }}">
                <i class="fas fa-fw fa-users"></i>
                <span>Data Dai</span></a>
        </li>

        <!-- Nav Item - Charts -->
        <li class="nav-item">
            <a class="nav-link" href="{{ route('role.index') }}">
                <i class=" fas fa-fw fa-user"></i>
                <span>Role</span></a>
        </li>

        <!-- Nav Item - Tables -->
        <li class="nav-item">
            <a class="nav-link" href="{{ route('users.index') }}">
                <i class="fas fa-fw fa-users"></i>
                <span>Users</span></a>
        </li>
    @elseif (Auth::user()->role_id == 2)
        <li class="nav-item">
            <a class="nav-link" href="{{ route('kegiatans.index') }}">
                <i class="fas fa-fw fa-chart-area"></i>
                <span>Data Kegiatan Dai</span></a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ route('mualafs.index') }}">
                <i class="fas fa-fw fa-chart-area"></i>
                <span>Data Mualaf</span></a>
        </li>
    @elseif (Auth::user()->role_id == 3)

    @else
    @endif
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true"
            aria-controls="collapsePages">
            <i class="fas fa-fw fa-user"></i>
            <span>Profile</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Profile:</h6>
                <a class="collapse-item" href="{{ route('profile.edit') }}">Profile</a>
                <a class="collapse-item" href="{{ route('user.password.edit') }}">Edit Password</a>
            </div>
        </div>
    </li>

    <hr class="sidebar-divider d-none d-md-block">

    <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal">
            <i class="fas fa-fw fa-sign-out-alt"></i>
            <span>Keluar</span>
        </a>
    </li>

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->

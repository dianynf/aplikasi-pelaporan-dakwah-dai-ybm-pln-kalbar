<!-- Favicons -->
<link href="{{ url('fronten/assets/img/favicon.ico') }}" rel="icon">
<link href="{{ url('fronten/assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

<!-- Google Fonts -->
<link
    href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
    rel="stylesheet">

<!-- Vendor CSS Files -->
<link href="{{ url('fronten/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ url('fronten/assets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
<link href="{{ url('fronten/assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
<link href="{{ url('fronten/assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
<link href="{{ url('fronten/assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
<link href="{{ url('fronten/assets/vendor/venobox/venobox.css') }}" rel="stylesheet">
<link href="{{ url('fronten/assets/vendor/animate.css/animate.min.css') }}" rel="stylesheet">
<link href="{{ url('fronten/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}"
    rel="stylesheet">

<!-- Template Main CSS File -->
<link href="{{ url('fronten/assets/css/style.css') }}" rel="stylesheet">

<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
<!-- Vendor JS Files -->
<script src="{{ url('fronten/assets/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ url('fronten/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ url('fronten/assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
<script src="{{ url('fronten/assets/vendor/php-email-form/validate.js') }}"></script>
<script src="{{ url('fronten/assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
<script src="{{ url('fronten/assets/vendor/venobox/venobox.min.js') }}"></script>
<script src="{{ url('fronten/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
<script src="{{ url('fronten/assets/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
<script src="{{ url('fronten/assets/vendor/counterup/counterup.min.js') }}"></script>
<script src="{{ url('fronten/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

<!-- Template Main JS File -->
<script src="{{ url('fronten/assets/js/main.js') }}"></script>

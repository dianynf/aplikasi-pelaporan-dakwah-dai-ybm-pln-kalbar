<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
<!-- Vendor JS Files -->
<script src="{{ url('fronten/assets/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ url('fronten/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ url('fronten/assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
<script src="{{ url('fronten/assets/vendor/php-email-form/validate.js') }}"></script>
<script src="{{ url('fronten/assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
<script src="{{ url('fronten/assets/vendor/venobox/venobox.min.js') }}"></script>
<script src="{{ url('fronten/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
<script src="{{ url('fronten/assets/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
<script src="{{ url('fronten/assets/vendor/counterup/counterup.min.js') }}"></script>
<script src="{{ url('fronten/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

<!-- Template Main JS File -->
<script src="{{ url('fronten/assets/js/main.js') }}"></script>

<script type='text/javascript'>
    $(document).ready(function() {

        // Department Change
        $('#sel_depart').change(function() {

            // Department id
            var id = $(this).val();

            // Empty the dropdown
            $('#sel_emp').find('option').not(':first').remove();

            // AJAX request
            $.ajax({
                url: 'getDistricts/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {

                    var len = 0;
                    if (response['data'] != null) {
                        len = response['data'].length;
                    }

                    if (len > 0) {
                        // Read data and create <option >
                        for (var i = 0; i < len; i++) {

                            var id = response['data'][i].id;
                            var name = response['data'][i].name;

                            var option = "<option value='" + id +
                                "'>" + name +
                                "</option>";

                            $("#sel_emp").append(option);
                        }
                    }

                }
            });
        });

    });

</script>

<script>
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(
            fileName);
    });

</script>

<script type='text/javascript'>
    $(document).ready(function() {

        // Department Change
        $('#sel_emp').change(function() {

            // Department id
            var id = $(this).val();

            // Empty the dropdown
            $('#sel_vil').find('option').not(':first').remove();

            // AJAX request
            $.ajax({
                url: 'getVillages/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {

                    var len = 0;
                    if (response['data'] != null) {
                        len = response['data'].length;
                    }

                    if (len > 0) {
                        // Read data and create <option >
                        for (var i = 0; i < len; i++) {

                            var id = response['data'][i].id;
                            var name = response['data'][i].name;

                            var option = "<option value='" + id +
                                "'>" + name +
                                "</option>";

                            $("#sel_vil").append(option);
                        }
                    }

                }
            });
        });

    });

</script>

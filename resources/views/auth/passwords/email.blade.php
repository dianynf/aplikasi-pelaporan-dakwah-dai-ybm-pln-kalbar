@extends('layouts.auth')
@section('Lupa Password', "Profile - Da'i YBM PLN KAL-BAR")
@section('content')

    <main id="main">
        <section id="event-login" class="event-login mt-5">
            <div class="container">
                <div class="card">
                    <div class="col-md-8 offset-md-2">
                        <div class="card-body shadow p-3 mb-5 bg-white rounded">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <form method="POST" action="{{ route('password.email') }}">
                                @csrf
                                <h3 class="col-md-12 mb-4 text-center mt-2">Lupa Password</h3>
                                <div class="form-group">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                        name="email" placeholder="E-Mail Address" value="{{ old('email') }}" required
                                        autocomplete="email" autofocus>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    <p> Masukkan email untuk recover password</p>
                                </div>
                                <button type="submit" class="btn btn-learn-btn col-md-4 offset-md-4 mb-3">
                                    Lupa Password
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@extends('layouts.auth')

@section('content')
    <div class="container">
        <div class="card o-hidden border-0 shadow-lg my-5 col-lg-11 mx-auto">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4"><b>Isi Lengkap Data Diri Anda</b></h1>
                            </div>
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="form-group" id="only-number">
                                    <input id="username" type="text"
                                        class="form-control @error('username') is-invalid @enderror" name="username"
                                        value="{{ old('username') }}" required autocomplete="username"
                                        placeholder="Nama Lengkap">
                                    @error('username')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                        name="email" value="{{ old('email') }}" required autocomplete="email"
                                        placeholder="E-Mail Address">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    <p> *Mohon isi email dengan benar, untuk proses aktivasi email memperlukan email yang
                                        valid.</p>
                                </div>
                                <div class="form-group" id="only-number">
                                    <input type="number"
                                        class="form-control input-number @error('nohp') is-invalid @enderror" required
                                        maxlength="13" id="nohp" name="nohp" placeholder="Nomor Telepon"
                                        value="{{ old('nohp') }}">
                                    @error('nohp')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <div class="input-group" id="show_hide_password">
                                        <input id="password" type="password"
                                            class="form-control @error('password') is-invalid @enderror" name="password"
                                            required autocomplete="new-password" placeholder="Password">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group" id="show_hide_passwordd">
                                        <input id="password-confirm" type="password" class="form-control"
                                            name="password_confirmation" required autocomplete="new-password"
                                            placeholder="Konfirmasi Password">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-7">
                                            <div class="form-check checkbox-warning-filled">
                                                <input type="checkbox" class="form-check-input filled-in btn-checbox"
                                                    id="orangeExample">
                                                <label class="form-check-label sm-register mt-2" for="orangeExample">Saya
                                                    Menyetujui
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-5" style="text-align: right;">
                                            @if (Route::has('password.request'))
                                                <a class="small link-lupp" style="text-decoration:none;"
                                                    href="{{ route('password.request') }}">
                                                    {{ __('Lupa Password?') }}
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn button-login btn-block">
                                    Daftar
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('title')
    Da'i YBM PLN Kalimantan Barat
@endsection

@section('content')
    <section class="inner-page">
        <div class="container">
        </div>
    </section>
    <style>
        /* Make the image fully responsive */
        .carousel-inner img {
            width: 100%;
            height: 100%;
        }

    </style>
    <div id="demo" class="carousel slide" data-ride="carousel">

        <!-- Indicators -->
        <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
            <li data-target="#demo" data-slide-to="3"></li>
        </ul>

        <!-- The slideshow -->
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{ asset('fronten/assets/img/slide/slide-1.jpg') }}" alt="Los Angeles">
            </div>
            <div class="carousel-item">
                <img src="{{ asset('fronten/assets/img/slide/slide-2.jpg') }}" alt="Chicago">
            </div>
            <div class="carousel-item">
                <img src="{{ asset('fronten/assets/img/slide/slide-3.jpg') }}" alt="New York">
            </div>
            <div class="carousel-item">
                <img src="{{ asset('fronten/assets/img/slide/slide-4.jpg') }}" alt="New York">
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon"></span>
        </a>
    </div>
    {{-- <section id="hero">
        <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

            <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

            <div class="carousel-inner" role="listbox">

                <!-- Slide 1 -->
                <div class="carousel-item active"
                    style="background-image: url({{ asset('fronten/assets/img/slide/slide-1.jpg') }})">
                </div>

                <!-- Slide 2 -->
                <div class="carousel-item"
                    style="background-image: url({{ asset('fronten/assets/img/slide/slide-2.jpg') }})">
                </div>

                <!-- Slide 3 -->
                <div class="carousel-item"
                    style="background-image: url({{ asset('fronten/assets/img/slide/slide-3.jpg') }})">
                </div>

            </div>

            <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon icofont-simple-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon icofont-simple-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

        </div>
    </section> --}}

    <main id="main">
        <section id="event-list" class="event-list mt-berita">
            <div class="container">
                <div class="section-title">
                    <h2 class="color-judul">Berita Dai</h2>
                    <p>Program Dakwah Dai Pedalaman yang di miliki oleh YBM PLN UIW Kalimantran Barat</p>
                </div>
                <div class="row">
                    <div class="col-md-4 d-flex align-items-stretch">
                        <div class="card">
                            <div class="card-img">
                                <img src="{{ asset('fronten/assets/img/gallery/berita1.png') }}" alt="...">
                            </div>
                            <div class="card-body">
                                <h6 class="card-title">Ustadz Yanto Dai pedalaman YBM PLN UIW Kalimantran Barat</h6>
                                <div class="text-center">
                                    <a href="{{ url('/berita/ustyanto') }}" class="btn btn-sm btn-berita">Selengkapnya</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex align-items-stretch">
                        <div class="card">
                            <div class="card-img">
                                <img src="{{ asset('fronten/assets/img/gallery/berita2.png') }}" alt="...">
                            </div>
                            <div class="card-body">
                                <h6 class="card-title">Program Dakwah "Beting to Bening"</h6>
                                <div class="text-center">
                                    <a href="{{ url('/berita/beting-to-bening') }}"
                                        class="btn btn-sm btn-berita">Selengkapnya</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex align-items-stretch">
                        <div class="card">
                            <div class="card-img">
                                <img src="{{ asset('fronten/assets/img/gallery/berita3.jpg') }}" alt="...">
                            </div>
                            <div class="card-body">
                                <h6 class="card-title">Program Dakwah, YBM PLN Adakan Training Imam dan Khatib</h6>
                                <div class="text-center">
                                    <a href="{{ url('/berita/training-imam-dan-khatib') }}"
                                        class="btn btn-sm btn-berita">Selengkapnya</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 text-center">
                <a href="{{ route('berita') }}" class="btn-learn-center">Lihat Seluruh Berita</a>
            </div>
        </section><!-- End Event List Section -->
        <!-- ======= Counts Section ======= -->
        <section id="counts" class="counts mt-5 mb-5">
            <div class="container mb-5 mt-4">
                <div class="section-title">
                    {{-- <h2 class="color-judul" style="color: #fff;">Persebaran Dai</h2> --}}
                </div>
                <div class="row counters">
                    <div class="col-sm text-center mt-mobile">
                        <h3>Jumlah Dai</h3>
                        <span data-toggle="counter-up">{{ $total_dai }}</span>
                    </div>

                    <div class="col-sm text-center mt-mobile">
                        <h3>Jumlah Kegiatan</h3>
                        <span data-toggle="counter-up">{{ $total_kegiatan }}</span>
                    </div>

                    <div class="col-sm text-center mt-mobile">
                        <h3>Jumlah Mualaf</h3>
                        <span data-toggle="counter-up">{{ $total_mualaf }}</span>
                    </div>
                </div>
            </div>
        </section><!-- End Counts Section -->
        <br><br>
        <!-- ======= My & Family Section ======= -->
        <section id="about" class="about mt-3">
            <div class="container">
                <div class="row content">
                    <div class="col-lg-6">
                        <!-- <img src="assets/img/about.jpg" class="img-fluid" alt=""> -->
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/hFX9HabnySQ?rel=0"
                                allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="col-lg-6 pt-4 pt-lg-0">
                        <h3>PROGRAM DAKWAH - USTADZ MUDZAKKIR</h3>
                        <p>
                            Tak kenal lelah berdakwah hingga pedalaman Kalimantan
                        </p>
                        <p>
                            Ustadz Mudzakkir, adalah salah satu ustadz yang berdakwah dipedalaman Kalimantan Barat, tak
                            kenal
                            lelah sejauh apapun jaraknya ustadz mudzakkir tetap semangat berdakwah. amar ma'ruf nahi
                            mungkar,
                            hal itulah yang mendasari Beliau untuk terus semangatt berdakwah. Sudah puluhan tahun beliau
                            berdakwah dipedalaman kalimantan.
                        </p>
                        <p>
                            Semoga Allah beri kesehatan dan keberkahan selalu kepada ustadz dan Allah balas kebaikan atas
                            dakwah yang sudah beliau lakukan.
                        </p>
                    </div>
                </div>

            </div>
        </section><!-- End My & Family Section -->

        <!-- ======= Recent Photos Section ======= -->
        <section id="recent-photos" class="recent-photos mt-3">
            <div class="container">

                <div class="section-title">
                    <h2 class="color-judul">KOLABORASI</h2>
                </div>

                <div class="owl-carousel recent-photos-carousel">
                    <a href="{{ asset('fronten/assets/img/logo/baznaz.png') }}" class="venobox"
                        data-gall="recent-photos-carousel">
                        <img style="padding: 30px;"
                            src="{{ asset('fronten/assets/img/logo/baznaz.png') }}" alt=""></a>
                    <a href="{{ asset('fronten/assets/img/logo/kpmp.png') }}" class="venobox"
                        data-gall="recent-photos-carousel">
                        <img style="padding: 30px;"
                            src="{{ asset('fronten/assets/img/logo/kpmp.png') }}" alt=""></a>
                    <a href="{{ asset('fronten/assets/img/logo/alzahar.png') }}" class="venobox"
                        data-gall="recent-photos-carousel">
                        <img style="padding: 30px;"
                            src="{{ asset('fronten/assets/img/logo/alzahar.png') }}" alt=""></a>
                    <a href="{{ asset('fronten/assets/img/logo/dmi.png') }}" class="venobox"
                        data-gall="recent-photos-carousel">
                        <img style="padding: 30px;"
                            src="{{ asset('fronten/assets/img/logo/dmi.png') }}" alt=""></a>
                    <a href="{{ asset('fronten/assets/img/logo/dompetdhuafa.png') }}" class="venobox"
                        data-gall="recent-photos-carousel">
                        <img style="padding: 30px;"
                            src="{{ asset('fronten/assets/img/logo/dompetdhuafa.png') }}" alt=""></a>
                    <a href="{{ asset('fronten/assets/img/logo/rumahzakat.png') }}" class="venobox"
                        data-gall="recent-photos-carousel">
                        <img style="padding: 30px;"
                            src="{{ asset('fronten/assets/img/logo/rumahzakat.png') }}" alt=""></a>
                    <a href="{{ asset('fronten/assets/img/logo/izi.png') }}" class="venobox"
                        data-gall="recent-photos-carousel">
                        <img style="padding: 30px;"
                            src="{{ asset('fronten/assets/img/logo/izi.png') }}" alt=""></a>
                    <a href="{{ asset('fronten/assets/img/logo/hidayatullah.png') }}" class="venobox"
                        data-gall="recent-photos-carousel">
                        <img style="padding: 30px;"
                            src="{{ asset('fronten/assets/img/logo/hidayatullah.png') }}" alt=""></a>
                </div>
            </div>
        </section>
    </main>
@endsection

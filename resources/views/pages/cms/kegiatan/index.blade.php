@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        @if (session()->has('sukses'))
            <div class="alert alert-info col-xl-12 alert-dismissible fade show" role="alert">
                {{ session()->get('sukses') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="card border-left-primary shadow mb-4">
            <div class="card-header py-3">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h5 class="m-0 font-weight-bold text-gray-800 mt-2">Data Kegiatan Da'i</h5>
                    @if (Auth::user()->role_id == 1)
                        <a href="{{ url('/kegiatan') }}" class="btn btn-sm btn-primary mt-2"
                            style="color: #fff; margin-left: auto;"><i class="fas fa-plus"></i>
                            &nbsp;Tambah</a>&nbsp;
                        <a href="{{ url('/kegiatan/export-excel') }}" class="btn btn-sm btn-success mt-2"
                            style="color: #fff"><i class="far fa-file-excel"></i>
                            &nbsp;Export Excel</a>
                    @else
                        <a href="{{ url('/kegiatan') }}" class="btn btn-sm btn-primary mt-2"
                                style="color: #fff; margin-left: auto;"><i class="fas fa-plus"></i>
                                &nbsp;Tambah</a>
                    @endif
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Ustadz</th>
                                <th>Tanggal</th>
                                <th>Jum Kajian</th>
                                <th>Jum Jamaah</th>
                                <th>Sasaran</th>
                                <th>Evaluasi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @forelse ($items as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->user->username }}</td>
                                    <td>{{ $item->tgl }}</td>
                                    <td>{{ $item->jml_kajian }}</td>
                                    <td>{{ $item->jml_jamaah }}</td>
                                    <td>{{ $item->sasaran }}</td>
                                    <td>{{ $item->evaluasi }}</td>
                                    <td>
                                        <a href="{{ route('kegiatans.show', $item->id) }}"
                                            class="btn btn-sm btn-primary">
                                            <i class="fa fa-search-plus"></i>
                                        </a>
                                        @if (Auth::user()->role_id == 1)
                                            <a href="{{ route('kegiatans.edit', $item->id) }}"
                                                class="btn btn-sm btn-info">
                                                <i class="fa fa-pencil-alt"></i>
                                            </a>
                                            {{-- <form action="{{ route('kegiatans.destroy', $item->id) }}" method="post"
                                                class="d-inline">
                                                @csrf
                                                @method('delete')
                                                <button class="btn btn-sm btn-danger">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </form> --}}
                                            <form method="POST" action="{{ route('kegiatans.destroy', $item->id) }}" class="d-inline">
                                                @csrf
                                                @method('delete')
                                                <input name="_method" type="hidden" value="DELETE">
                                                <button type="submit" class="btn btn-sm btn-danger btn-flat show_confirm" data-toggle="tooltip" title='Delete'><i class="fa fa-trash"></i></button>
                                            </form>
                                            @else
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <td colspan="7" class="text-center">
                                    Data Kosong
                                </td>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>

<body>
    <table>
        <thead>
            <tr>
                <th style="text-align: right;" rowspan="4"></th>
                <th valign="center" colspan="5"
                    style="font-weight: bold; text-align: center; border: 2px solid #ffffff;">YAYASAN BAITUL MAAL </th>
            </tr>
            <tr>
                <th valign="center" colspan="5"
                    style="font-weight: bold; text-align: center; border: 2px solid #ffffff;">PLN WILAYAH KALIMANTAN
                    BARAT</th>
            </tr>
            <tr>
                <th valign="center" colspan="5"
                    style="font-weight: bold; text-align: center; border: 2px solid #ffffff;">Jalan Adisucipto Km 7,3
                    Sei Raya</th>
            </tr>
            <tr>
                <th valign="center" colspan="5"
                    style="font-weight: bold; text-align: center; border: 2px solid #ffffff;">KABUPATEN KUBU RAYA
                </th>
            </tr>
            <tr>
                <td style="border: 2px solid #ffffff;"></td>
                <td style="border: 2px solid #ffffff;"></td>
                <td style="border: 2px solid #ffffff;"></td>
                <td style="border: 2px solid #ffffff;"></td>
                <td style="border: 2px solid #ffffff;"></td>
            </tr>
            <tr>
                <th style="width: 20px; font-weight: bold; border: 2px solid #000000; text-align: center;">Nama Ustadz
                </th>
                <th style="width: 20px; font-weight: bold; border: 2px solid #000000; text-align: center;">Tanggal</th>
                <th style="width: 20px; font-weight: bold; border: 2px solid #000000; text-align: center;">Kegiatan
                    Belajar/Materi
                </th>
                <th style="width: 20px; font-weight: bold; border: 2px solid #000000; text-align: center;">Sasaran
                </th>
                <th style="width: 20px; font-weight: bold; border: 2px solid #000000; text-align: center;">Jumlah Kajian
                </th>
                <th style="width: 20px; font-weight: bold; border: 2px solid #000000; text-align: center;">Jumlah Jamaah
                </th>
                <th style="width: 20px; font-weight: bold; border: 2px solid #000000; text-align: center;">Evaluasi
                </th>
                <th style="width: 20px; font-weight: bold; border: 2px solid #000000; text-align: center;">Alamat</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($export_excel as $kegiatan)
                <tr>
                    <td style="border: 2px solid #000000;">{{ $kegiatan->user->username }}</td>
                    <td style="border: 2px solid #000000;">{{ $kegiatan->tgl }}</td>
                    <td style="border: 2px solid #000000;">{{ $kegiatan->jml_kajian }}</td>
                    <td style="border: 2px solid #000000;">{{ $kegiatan->jml_jamaah }}</td>
                    <td style="border: 2px solid #000000;">{{ $kegiatan->deskripsi }}</td>
                    <td style="border: 2px solid #000000; width:50px;">{{ $kegiatan->alamat }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

</body>

</html>

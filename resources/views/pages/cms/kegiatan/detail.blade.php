@extends('layouts.admin')
@section('content')
    <div class="container-fluid">
        <div class="row mt-12">
            <div class="col md-5">
                <div class="card-deck">
                    <div class="col-xl-12 col-md-8 mb-5">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                        <h3 class="h3 mb-0 text-gray-800">Detail Kegiatan</h3>
                                    </div>
                                    <!-- Content Row -->
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Nama Ustadz</th>
                                            <td>{{ $item->user->username }}<b>
                                        </tr>
                                        <tr>
                                            <th>Nomor Handphone</th>
                                            <td>{{ $item->user->nohp }}<b>
                                        </tr>
                                        <tr>
                                            <th>Alamat Ustadz</th>
                                            <td>{{ $item->user->alamat }}<b>
                                        </tr>
                                        <tr>
                                            <th>Tanggal</th>
                                            <td>{{ $item->tgl }}</td>
                                        </tr>
                                        <tr>
                                            <th width="25%">Kegiatan Belajar/Materi</th>
                                            <td>{{ $item->deskripsi }}</td>
                                        </tr>
                                        <tr>
                                            <th>Sasaran</th>
                                            <td>{{ $item->sasaran }}</td>
                                        </tr>
                                        <tr>
                                            <th>Jumlah Kajian</th>
                                            <td class="text-orange">{{ $item->jml_kajian }}</td>
                                        </tr>
                                        <tr>
                                            <th>Jumlah Jama'ah</th>
                                            <td class="text-orange">{{ $item->jml_jamaah }}</td>
                                        </tr>
                                        <tr>
                                            <th>Evaluasi</th>
                                            <td>{{ $item->evaluasi }}</td>
                                        </tr>
                                        <tr>
                                            <th>Alamat item</th>
                                            <td class="lower">{{ $item->alamat }}. <br>
                                                Desa {{ $item->desa->name }}. KEC {{ $item->kec->name }}. KAB
                                                {{ $item->kab->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Dokumentasi</th>
                                            <td>
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th>No</th>
                                                        <th>File</th>
                                                    </tr>
                                                    @php $no = 1; @endphp
                                                    @foreach ($item->fotos as $detail)
                                                        <tr>
                                                            <td>{{ $no++ }}</td>
                                                            <td>
                                                                <div style="width: 25rem;">
                                                                    <img src="{{ Storage::url('dokumentasi/' . $detail->foto) }}"
                                                                        class="img-thumbnail">
                                                                    <br>{{ $detail->foto }}
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

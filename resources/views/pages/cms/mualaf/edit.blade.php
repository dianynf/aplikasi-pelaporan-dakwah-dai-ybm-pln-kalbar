@extends('layouts.admin')
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Content Row -->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="mt-12 row">
            <div class="col md-5">
                <div class="card-deck">
                    <div class="mb-5 col-xl-12 col-md-8">
                        <div class="py-2 shadow card border-left-primary h-100">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="modal-body">
                                        <h3 class="mb-4 text-gray-800 h3">Edit Data</h3>
                                        <hr>
                                        <form method="POST" action="{{ route('mualafs.update', $mualaf->id) }}" enctype="multipart/form-data">
                                            @method('PUT')
                                            @csrf
                                            <div class="form-group row">
                                                <label for="tgl" class="col-sm-2 col-form-label">Tanggal</label>
                                                <div class="col-sm-10">
                                                    <input id="tgl" type="text"
                                                        class="form-control datepicker @error('tgl') is-invalid @enderror"
                                                        name="tgl" value="{{ old('tgl', $mualaf->tgl) }}"
                                                        autocomplete="tgl" autofocus>
                                                    @error('tgl')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="jml_mualaf" class="col-sm-2 col-form-label">Jumlah
                                                    Mualaf</label>
                                                <div class="col-sm-10">
                                                    <input id="jml_mualaf" type="number" class="form-control"
                                                        name="jml_mualaf"
                                                        value="{{ old('jml_mualaf', $mualaf->jml_mualaf) }}"
                                                        autocomplete="jml_mualaf" autofocus>
                                                    @error('jml_mualaf')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group row">
                                                <div class="col-sm-2">
                                                    <h6>Data Mualaf</h6>
                                                </div>
                                                <div class="col-sm-10">
                                                    @foreach ($mualaf->fotos as $detail)
                                                    <input type="hidden" name="id[]" value="{{$detail->id}}">
                                                    <div class="mt-4 card">
                                                        <div class="card-body">
                                                            <div class="form-group row">
                                                                <div class="mt-2 col-md-6">
                                                                    <label>Nama Mualaf</label>
                                                                    <div class="input-group realprocode control-group lst increment">
                                                                        <div class="custom-file">
                                                                            <input type="text" name="nama[]" class="form-control" id="nama"
                                                                                placeholder="Nama" value="{{ old('nama', $detail->nama) }}"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-2 col-md-6 form-group">
                                                                    <label>Alamat</label>
                                                                    <input type="text" required class="form-control" name="alamat[]" id="alamat"
                                                                        placeholder="Alamat" data-rule="alamat"
                                                                        data-msg="Please enter a valid alamat" value="{{  old('alamat', $detail->alamat) }}"/>
                                                                    <div class="validate"></div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-md-4 form-group">
                                                                    <label>Foto Dokumentasi</label>
                                                                    <div class="form-group">
                                                                        <input type="file" name="foto[]" class="form-control-file"
                                                            id="exampleFormControlFile1" value="{{ $detail->foto }}"><br>
                                                                        <div class="form-group">
                                                                            <img src="{{ Storage::url('mualaf/' . $detail->foto) }}" class="img-thumbnail" width="50%">
                                                                            <p>{{ $detail->foto }}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-8 form-group">
                                                                    <label>Ceritakan Bagaimana Mengenal Islam</label>
                                                                    <textarea class="form-control" name="deskripsi[]" required="required" rows="6"
                                                                        value="{{ old('deskripsi', $detail->deskripsi) }}">{{ $detail->deskripsi }}</textarea>
                                                                    @error('deskripsi')
                                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="form-file row justify-content-end">
                                                <div class="col-sm-10">
                                                    <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection

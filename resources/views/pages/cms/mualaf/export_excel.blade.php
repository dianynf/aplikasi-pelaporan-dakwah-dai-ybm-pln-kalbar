<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>

<body>
    <table>
        <thead>
            <tr>
                <th style="text-align: right;" rowspan="4"></th>
                <th valign="center" colspan="4"
                    style="font-weight: bold; text-align: center; border: 2px solid #ffffff;">YAYASAN BAITUL MAAL </th>
            </tr>
            <tr>
                <th valign="center" colspan="4"
                    style="font-weight: bold; text-align: center; border: 2px solid #ffffff;">PLN WILAYAH KALIMANTAN
                    BARAT</th>
            </tr>
            <tr>
                <th valign="center" colspan="4"
                    style="font-weight: bold; text-align: center; border: 2px solid #ffffff;">Jalan Adisucipto Km 7,3
                    Sei Raya</th>
            </tr>
            <tr>
                <th valign="center" colspan="4"
                    style="font-weight: bold; text-align: center; border: 2px solid #ffffff;">KABUPATEN KUBU RAYA
                </th>
            </tr>
            <tr>
                <td style="border: 2px solid #ffffff;"></td>
                <td style="border: 2px solid #ffffff;"></td>
                <td style="border: 2px solid #ffffff;"></td>
                <td style="border: 2px solid #ffffff;"></td>
                <td style="border: 2px solid #ffffff;"></td>
            </tr>
            <tr>
                <th style="width: 20px; font-weight: bold; border: 2px solid #000000; text-align: center;">Nama Ustadz
                </th>
                <th style="width: 20px; font-weight: bold; border: 2px solid #000000; text-align: center;">Tanggal</th>
                <th style="width: 20px; font-weight: bold; border: 2px solid #000000; text-align: center;">Jumlah Mualaf
                </th>
                <th style="width: 20px; font-weight: bold; border: 2px solid #000000; text-align: center;">Alamat
                </th>
                <th style="width: 20px; font-weight: bold; border: 2px solid #000000; text-align: center;">Deskripsi
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($export_excel as $mualaf)
                <tr>
                    <td style="border: 2px solid #000000;">{{ $mualaf->user->username }}</td>
                    <td style="border: 2px solid #000000;">{{ $mualaf->tgl }}</td>
                    <td style="border: 2px solid #000000;">{{ $mualaf->jml_mualaf }}</td>
                    <td style="border: 2px solid #000000;">{{ $mualaf->user->alamat }}</td>
                    <td style="border: 2px solid #000000; width:50px;">{{ $mualaf->deskripsi }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

</body>

</html>

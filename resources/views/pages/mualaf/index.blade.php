@extends('layouts.app')

@section('title')
    Mualaf
@endsection

@section('content')
    <main id="main">
        <section id="event-login" class="mt-5 event-login">
            <div class="container">
                <div class="mt-2 card">
                    <div class="col-md-12">
                        <div class="p-3 mb-5 bg-white rounded shadow card-body">
                            @if (session()->has('sukses'))
                                <div class="alert alert-info col-xl-12 alert-dismissible fade show" role="alert">
                                    {{ session()->get('sukses') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <form method="POST" action="mualaf/create" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <h3 class="mt-5 mb-5 col">Input Data Mualaf dari {{ Auth::user()->username }}
                                </h3>
                                <hr>
                                @if (Auth::user()->role_id == 1)
                                    <div class="mt-4 col form-group">
                                        <label>Pilih Dai</label>
                                        <select required class="form-control" name="users_id" id="users_id">
                                            <option value="0">Pilih Dai</option>
                                            @foreach ($daiData['data'] as $dais)
                                                <option value='{{ $dais->id }}'>{{ $dais->username }}</option>
                                            @endforeach
                                        </select>
                                        @error('users_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                @endif
                                <div class="mt-4 col form-group">
                                    <label>Tanggal</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker" name="tgl"
                                            value="{{ old('tgl') }}"  required>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="icofont-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                    @error('tgl')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <script type="text/javascript">
                                    $(function() {
                                        $(".datepicker").datepicker({
                                            format: '  dd/mm/yyyy',
                                            autoclose: true,
                                            todayHighlight: true,
                                        });
                                    });
                                </script>
                                <div class="mt-4 col form-group">
                                    <label>Jumlah Mualaf</label>
                                    <input type="number" class="form-control" name="jml_mualaf"
                                        value="{{ old('jml_mualaf') }}" required autofocus>
                                    @error('jml_mualaf')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="mt-4 col form-group">
                                    <label>Data Mualaf</label>
                                    <div class="form-row">
                                        <div class="mt-2 col-md-4">
                                            <div class="input-group realprocode control-group lst increment">
                                                <div class="input-group-prepend">
                                                    <button class="btn btn-primary" type="button" id="add_button"><i
                                                            class="icofont-plus-square"></i></button>
                                                </div>
                                                <div class="custom-file">
                                                    <input type="text" name="nama[]" class="form-control" required id="nama"
                                                        placeholder="Nama" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mt-2 col-md-4 form-group">
                                            <input type="text" required class="form-control" name="alamat[]" id="alamat"
                                                placeholder="Alamat" data-rule="alamat"
                                                data-msg="Please enter a valid alamat" />
                                            <div class="validate"></div>
                                        </div>
                                        <div class="mt-2 col-md-4 form-group">
                                            <div class="form-group">
                                                <div class="input-group realprocode control-group lst increment">
                                                    <div class="custom-file">
                                                        <input type="file" name="foto[]" class="form-control-file">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col form-group">
                                            <label>Ceritakan Bagaimana Mengenal Islam</label>
                                            <textarea class="form-control" name="deskripsi[]" required="required" rows="6"
                                                value="{{ old('deskripsi') }}"  required></textarea>
                                            @error('deskripsi')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="field_wrapper">
                                    </div>

                                    <div class="mt-4 form-row realprocode control-group lst increment">
                                        <div class="col-md-4">
                                            <div class="form-group ">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <button class="btn btn-danger" type="button"><i
                                                                class="icofont-trash fldemo glyphicon glyphicon-remove"></i></button>
                                                    </div>
                                                    <div class="custom-file">
                                                        <input type="text" name="nama[]" class="form-control" id="nama"
                                                            placeholder="Nama" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <input type="text" required class="form-control" name="alamat[]" id="alamat"
                                                placeholder="Alamat" data-rule="alamat"
                                                data-msg="Please enter a valid alamat" />
                                            <div class="validate"></div>
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="file" name="foto[]" class="form-control-file"
                                                            id="exampleFormControlFile1">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-5 col form-group">
                                            <label>Ceritakan Bagaimana Mengenal Islam</label>
                                            <textarea class="form-control" name="deskripsi[]" required="required" rows="6"
                                                value="{{ old('deskripsi') }}"></textarea>
                                            @error('deskripsi')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col form-group">
                                    <button type="submit"
                                        class="mt-4 mb-3 btn btn-learn-btn col-md-4 offset-md-4">Kirim</button>
                                </div>
                            </form>
                        </div>
                        <script type="text/javascript">
                            $(document).ready(function() {
                                var maxField = 10; //Input fields increment limitation
                                var addButton = $('#add_button'); //Add button selector
                                var wrapper = $('.field_wrapper'); //Input field wrapper
                                var fieldHTML =
                                    '<div class="mt-4 form-row realprocode control-group lst increment"><div class="col-md-4"><div class="field_wrapper"><div class="form-group"><div class="input-group"><div class="input-group-prepend"><button class="btn btn-danger" type="button"><i class="icofont-trash fldemo glyphicon glyphicon-remove"></i></button></div><div class="custom-file"><input type="text" name="nama[]" class="form-control" id="nama" placeholder="Nama" /></div></div></div></div></div>';
                                fieldHTML = fieldHTML +
                                    '<div class="col-md-4 form-group"><input type="text" required class="form-control" name="alamat[]" id="alamat" placeholder="Alamat" data-rule="alamat" data-msg="Please enter a valid alamat" /><div class="validate"></div></div>';
                                fieldHTML = fieldHTML +
                                    '<div class="col-md-4 form-group"><div class="form-group"><div class="input-group"><div class="custom-file"><input type="file" name="foto[]" class="form-control-file" id="exampleFormControlFile1"></div></div></div></div>';
                                fieldHTML = fieldHTML +
                                    '<div class="mb-3 col form-group"><label>Ceritakan Bagaimana Mengenal Islam</label><textarea class="form-control" name="deskripsi[]" required="required" rows="6"value="{{ old('deskripsi') }}"></textarea>@error('deskripsi')<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror</div></div></div><hr>';
                                var x = 1; //Initial field counter is 1

                                //Once add button is clicked
                                $(addButton).click(function() {
                                    //Check maximum number of input fields
                                    if (x < maxField) {
                                        x++; //Increment field counter
                                        $(wrapper).append(fieldHTML); //Add field html
                                    }
                                });

                                $("body").on("click", ".btn-danger", function() {
                                    $(this).parents(".realprocode").remove();
                                });
                            });

                        </script>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

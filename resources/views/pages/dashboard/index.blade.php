@extends('layouts.dashboard')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Content Row -->
        <div class="row col-mb-12">
            <!-- Earnings (Monthly) Card Example -->
            @if (Auth::user()->role_id == 1)
                <div class="mt-2 col-xl-4 col-md-6">
                    <div class="shadow card border-left-primary">
                        <div class="card-body">
                            <h6 class="text-center text-gray-800 h6 font-weight-bold">Jumlah Wilayah</h6>
                            <div class="mt-3 text-center">
                                <div class="row">
                                    <div class="col-sm">
                                        Kabupaten <br><b>{{ $total_kab }}</b>
                                    </div>
                                    <div class="col-sm">
                                        Kecamatan <br><b>{{ $total_kec }}</b>
                                    </div>
                                    <div class="col-sm">
                                        Desa <br><b>{{ $total_desa }}</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mt-2 col-xl-2 col-md-6">
                    <div class="shadow card border-left-info">
                        <div class="card-body">
                            <h6 class="text-center text-gray-800 h6 font-weight-bold">Jumlah Da'i</h6>
                            <div class="mt-4 text-center">
                                <h3 class="font-weight-bold">{{ $total_dai }}</h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mt-2 col-xl-3 col-md-6">
                    <div class="shadow card border-left-success">
                        <div class="card-body">
                            <h6 class="text-center text-gray-800 h6 font-weight-bold">Jumlah Kegiatan</h6>
                            <div class="mt-4 text-center">
                                <h3 class="font-weight-bold">{{ $total_kegiatan }}</h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mt-2 col-xl-3 col-md-6">
                    <div class="shadow card border-left-success">
                        <div class="card-body">
                            <h6 class="text-center text-gray-800 h6 font-weight-bold">Jumlah Mualaf</h6>
                            <div class="mt-4 text-center">
                                <h3 class="font-weight-bold">{{ $total_mualaf }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            @elseif (Auth::user()->role_id == 2)
                <div class="mt-2 col-xl-4 col-md-6">
                    <div class="shadow card border-left-primary">
                        <div class="card-body">
                            <h6 class="text-center text-gray-800 h6 font-weight-bold">Jumlah Wilayah</h6>
                            <div class="mt-3 text-center">
                                <div class="row">
                                    <div class="col-sm">
                                        Kabupaten <br><b>{{ $total_kab }}</b>
                                    </div>
                                    <div class="col-sm">
                                        Kecamatan <br><b>{{ $total_kec }}</b>
                                    </div>
                                    <div class="col-sm">
                                        Desa <br><b>{{ $total_desa }}</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-2 col-xl-4 col-md-6">
                    <div class="shadow card border-left-primary">
                        <div class="card-body">
                            <h6 class="text-center text-gray-800 h6 font-weight-bold">Jumlah Da'i</h6>
                            <div class="mt-4 text-center">
                                <h3 class="font-weight-bold">{{ $total_dai }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-2 col-xl-4 col-md-6">
                    <div class="shadow card border-left-success">
                        <div class="card-body">
                            <h6 class="text-center text-gray-800 h6 font-weight-bold">Jumlah Mualaf</h6>
                            <div class="mt-4 text-center">
                                <h3 class="font-weight-bold">{{ $total_mualaf }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            @else
            @endif
        </div>
        <div class="mt-4 row">
            <div class="col-xl-12 col-lg-7">
                <div class="mb-4 shadow card border-left-primary">
                    <div class="py-3 card-header">
                        <h5 class="m-0 text-gray-800 font-weight-bold">Kegiatan Da'i 2022</h5>
                    </div>
                    <div class="card-body">
                        <div class="chart-bar">
                            <canvas id="kegiatanChart"></canvas>
                        </div>
                        <hr>
                    </div>
                </div>

            </div>
        </div>
        <div class="mt-2 row">
            <div class="col-xl-12 col-lg-7">
                <div class="mb-4 shadow card border-left-success">
                    <div class="py-3 card-header">
                        <h5 class="m-0 text-gray-800 font-weight-bold">Data Mualaf 2022</h5>
                    </div>
                    <div class="card-body">
                        <div class="chart-bar">
                            <canvas id="myMualafs"></canvas>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    @endsection

@extends('layouts.app')

@section('title')
    Kegiatan Da'i
@endsection

@section('content')
    <main id="main">
        <section id="event-login" class="mt-5 event-login">
            <div class="container">
                <div class="card">
                    <div class="col-md-12">
                        <div class="p-3 mb-5 bg-white rounded shadow card-body">
                            @if (session()->has('sukses'))
                                <div class="alert alert-info col-xl-12 alert-dismissible fade show" role="alert">
                                    {{ session()->get('sukses') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            @if (session()->has('gagal'))
                                <div class="alert alert-info col-xl-12 alert-dismissible fade show" role="alert">
                                    {{ session()->get('gagal') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <form method="POST" action="/kegiatan/store" enctype="multipart/form-data">
                                @csrf
                                <h2 class="mt-4 mb-4 col">Input Kegiatan {{ Auth::user()->username }}
                                </h2>
                                <hr>
                                @if (Auth::user()->role_id == 1)
                                    <div class="mt-4 col form-group">
                                        <label>Pilih Dai</label>
                                        <select required class="form-control" name="users_id" id="users_id" autofocus>
                                            <option value="0">Pilih Dai</option>
                                            @foreach ($daiData['data'] as $dais)
                                                <option value='{{ $dais->id }}'>{{ $dais->username }}</option>
                                            @endforeach
                                        </select>
                                        @error('users_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                @endif
                                <div class="col form-group">
                                    <label>Tanggal</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker" name="tgl"
                                            value=" {{ old('tgl') }}" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="icofont-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                @error('tgl')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <script type="text/javascript">
                                    $(function() {
                                        $(".datepicker").datepicker({
                                            format: '  dd/mm/yyyy',
                                            autoclose: true,
                                            todayHighlight: true,
                                        });
                                    });

                                </script>
                                <div class="col form-group">
                                    <label>Kegiatan Belajar/Materi</label>
                                    <textarea class="form-control" name="deskripsi" required autofocus required="required" rows="6"
                                        value="{{ old('deskripsi') }}"></textarea>
                                    @error('deskripsi')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col form-group">
                                    <label>Sasaran</label>
                                    <input class="form-control" name="sasaran" value="{{ old('sasaran') }}" required autofocus>
                                    @error('sasaran')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="mt-4 col form-group">
                                    <label>Jumlah Kajian dalam Satu Bulan</label>
                                    <input type="number" class="form-control" name="jml_kajian"
                                        value="{{ old('jml_kajian') }}" required autofocus>
                                    @error('jml_kajian')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="mt-4 col form-group">
                                    <label>Jumlah Jamaah Kajian</label>
                                    <input type="number" class="form-control" name="jml_jamaah"
                                        value="{{ old('jml_jamaah') }}" required autofocus>
                                    @error('jml_jamaah')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col form-group">
                                    <label>Evaluasi Penilaian</label>
                                    <input class="form-control" name="evaluasi" value="{{ old('evaluasi') }}" required>
                                    @error('evaluasi')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col form-group">
                                    <label>Foto Kegiatan <span style="color: #ff0000">*</span></label>
                                    <div class="field_wrapper">
                                        <div class="form-group">
                                            <div class="mt-2 input-group realprocode control-group lst increment">
                                                <div class="custom-file">
                                                    <input type="file" name="foto[]" id="foto" class="custom-file-input" required>
                                                    <label class="custom-file-label">Upload File</label>
                                                </div>
                                            </div>
                                            <div class="mt-2 input-group realprocode control-group lst increment">
                                                <div class="custom-file">
                                                    <input type="file" name="foto[]" id="foto" class="custom-file-input">
                                                    <label class="custom-file-label">Upload File</label>
                                                </div>
                                            </div>
                                            <div class="mt-2 input-group realprocode control-group lst increment">
                                                <div class="custom-file">
                                                    <input type="file" name="foto[]" id="foto" class="custom-file-input">
                                                    <label class="custom-file-label">Upload File</label>
                                                </div>
                                            </div>
                                            <div class="mt-2 input-group realprocode control-group lst increment">
                                                <div class="custom-file">
                                                    <input type="file" name="foto[]" id="foto" class="custom-file-input">
                                                    <label class="custom-file-label">Upload File</label>
                                                </div>
                                            </div>
                                            <div class="mt-2 input-group realprocode control-group lst increment">
                                                <div class="custom-file">
                                                    <input type="file" name="foto[]" id="foto" class="custom-file-input">
                                                    <label class="custom-file-label">Upload File</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span style="color: #d06819;">
                                        Gambar yang Anda upload sebaiknya memiliki
                                        berukuran tidak lebih dari 2MB.
                                    </span>
                                </div>

                                <div class="mt-4 col form-group">
                                    <label>Alamat Kegiatan</label>
                                    <input type="text" class="form-control" name="alamat" value="{{ old('alamat') }}"
                                        required autofocus>
                                    @error('alamat')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col form-group">
                                    <label>Pilih Kabupaten/Kota</label>
                                    <select class="form-control" id='sel_depart' name="id_kab">
                                        <option value="0">Kabupaten/Kota</option>
                                        @foreach ($departmentData['data'] as $department)
                                            <option value='{{ $department->id }}'>{{ $department->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col form-group">
                                    <label>Pilih Kecamatan</label>
                                    <select class="form-control" name="id_kec" id='sel_emp'>
                                        <option value='0'>Kecamatan</option>
                                    </select>
                                </div>
                                <div class="col form-group">
                                    <label>Pilih Kelurahan/Desa</label>
                                    <select class="form-control" name="id_desa" id='sel_vil'>
                                        <option value='0'>Desa</option>
                                    </select>
                                </div>
                                <div class="col form-group">
                                    <button type="submit"
                                        class="mt-4 mb-3 btn btn-learn-btn col-md-4 offset-md-4">Kirim</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

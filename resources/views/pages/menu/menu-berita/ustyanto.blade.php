@extends('layouts.main')
@section('title', 'Ustad Yanto Dai Pedalaman YBM PLN KAL-BAR')

@section('content')
    <main id="main">
        <section class="inner-page">
            <div class="container">
            </div>
        </section>
        <section id="story-intro" class="story-intro">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="card mb-3">
                            <img class="card-img-top center mt-5"
                                style="width: 70%; display: block; margin-left: auto; margin-right: auto;"
                                src="{{ asset('fronten/assets/img/gallery/berita1.png') }}" alt="...">
                            <div class="card-body mt-5">
                                <h2 class="card-title">Ustad Yanto</h2>
                                <p class="card-text">
                                    Ustad Yanto nama salah satu dari 29 dai pedalaman yang di miliki oleh YBM PLN UIW
                                    Kalimantran Barat. Beliau tinggal di dusun Engkalau desa Rahayu Kecamatan Parindu Kab.
                                </p>
                                <p>
                                    Sanggau Kalimantan Barat. Dusun ini berisi kaum muslim yang jumlahnya minoritas.
                                    Akses paling cepat dan agak bagus menuju desa ini harus melalui perkebunan kelapa
                                    sawit dan ditempuh kurang lebih 1 jam dari jalan raya terdekat.
                                </p>
                                <p>
                                    Da’i-da’i pedalaman ini berada di daerah-daerah yang membutuhkan dakwah lebih intensif.
                                    Ustad Yanto sudah 3 tahun menjadi da’i pedalaman YBM PLN ini, sehari-hari beliau
                                    mengajarkan mengaji para anak-anak di desa Rahayu. Selain mengajar mengaji anak-anak,
                                    ustad Yanto pun ada pengajian dan pertemuan dengan orang-orang dewasa setiap pekannya.
                                    Selain mengajar mengaji, beliau pun menjadi guru honorer di salah satu SD Negeri
                                    terdekat
                                    dari desa Rahayu. Beliau mengajar Pendidikan agama Islam, karena sebelum beliau mengajar
                                    anak-anak yang beragama Islam ketika pelajaran agama di ajar oleh guru agama lain. <br>
                                    Selain membimbing mengenai agama, ustad Yanto pun bersama warga membentuk kelompok usaha
                                    yang bernama kelompok usaha baru II, kegiatan yang dilakukan diantaranya adalah berkebun
                                    sayuran dan juga menernak ikan lele. Mereka yang tergabung di kelompok usaha ini adalah
                                    warga dusun Engkalau yang muslim. “Saya merasa terpanggil dengan keadaan kaum muslim di
                                    dusun ini, yang mayoritas mereka kurang paham mengenai ilmu agama” ujar Ustad Yanto
                                </p>
                                <p style="color:blue;">
                                    #ybmpln #menjejakmanfaat #daipedalaman #dakwah
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@extends('layouts.main')
@section('title', 'Program Dakwah, YBM PLN Adakan Training Imam dan Khatib')

@section('content')
    <main id="main">
        <section class="inner-page">
            <div class="container">
            </div>
        </section>
        <section id="story-intro" class="story-intro">
            <div class="container">
                <div class="card mb-3">
                    <img class="card-img-top center mt-5"
                        style="width: 60%; display: block; margin-left: auto; margin-right: auto;"
                        src="{{ asset('fronten/assets/img/gallery/berita2.png') }}" alt="...">
                    <div class="card-body mt-5">
                        <h2 class="card-title">YBM PLN Kalbar Serahkan Bantuan Motor Ambulans dan Bantuan Dana Dakwah untuk
                            Program ‘Beting to Bening’</h2>
                        <p class="card-text">
                            BAGI Tari, bantuan tersebut sangat membantu dirinya dalam memberikan layanan kesehatan kepada
                            masyarakat di daerah-daerah terpencil. “Alhamdulillah, saya sangat bersyukur karena telah
                            diberikan bantuan motor ambulans dari YBM PLN Kalbar. Semoga bantuan ini dapat mendatangkan
                            keberkahan buat saya dan buat seluruh karyawan PLN Kalbar,” ungkap Tari.</p>
                        <p>
                            Selain bantuan sepeda motor ambulans, YBM PLN Kalbar juga menyerahkan bantuan dana dakwah untuk
                            program ‘Beting to Bening’ yang dilaksanakan oleh Baznas Kalbar.

                        </p>
                        <p>
                            Ketua YBM PLN Kalbar, Andang Triwahyudi berharap bantuan sepeda motor ambulans dapat membantu
                            meningkatkan layanan kesehatan kepada masyarakat, khususnya yang berada di daerah terpencil.
                            Sementara bantuan dana dakwah tersebut digunakan untuk mendorong kegiatan-kegiatan dakwah dan
                            pendidikan untuk warga guna menciptakan lingkungan yang aman, damai, dan harmonis di Kampung
                            Beting.
                        </p>
                        <p>
                            “Kami berharap bantuan yang diberikan dapat memberikan manfaat bagi para mustahik dan
                            mendatangkan keberkahan bagi seluruh karyawan PLN yang telah secara sukarela memberikan sebagian
                            penghasilannya yang telah kami kelola sesuai dengan syariat pengelolaan ZIS,” kata Andang.
                        </p>
                        <p>
                            Selama pandemi Covid-19, YBM PLN Kalbar juga telah menyerahkan bantuan paket sembako untuk warga
                            kurang mampu di seluruh wilayah Kalbar. Bantuan paket sembako pertama diserahkan sebanyak 800
                            paket, selanjutnya 1440 paket sembako diserahkan saat Ramadhan.
                        </p>
                        <p>
                            Di bidang pendidikan, YBM PLN Kalbar juga telah mengirimkan para santri untuk belajar di
                            Pesantren Teknologi Informasi dan Komunikasi (PETIK), kursus keterampilan di Rumah Gemilang
                            Indonesia (RGI), dan sekolah programmer selama 3 bulan.
                        </p>
                        <p>
                            Senada, Ordaia Arqam Nja&#8217;Oemar, senior Manager SDM dan Umum PLN Kalbar menegaskan bahwa
                            PLN Kalbar berkomitmen untuk terus memberikan bantuan kepada masyarakat yang membutuhkan,
                            terutama di masa pandemi Covid-19 ini.
                        </p>
                        <p>
                            Kami berharap keberadaan PLN dapat memberikan manfaat bagi warga sekitar, terutama untuk warga
                            yang kurang mampu melalui program-program sosial yang dilaksanakan oleh YBM. Bantuan yang
                            diberikan akan terus kami evaluasi dan sebarkan di seluruh daerah di mana PLN hadir disana,”
                            pungkas Arqam.
                        </p>
                        <p style="color:blue;">
                            Sumber : Pontianak Post<br>
                            https://pontianakpost.co.id/ybm-pln-kalbar-serahkan-bantuan-motor-ambulans/
                        </p>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

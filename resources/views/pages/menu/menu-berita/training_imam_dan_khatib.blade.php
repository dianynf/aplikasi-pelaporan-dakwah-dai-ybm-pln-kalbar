@extends('layouts.main')
@section('title', 'Program Dakwah, YBM PLN Adakan Training Imam dan Khatib')

@section('content')
    <main id="main">
        <section class="inner-page">
            <div class="container">
            </div>
        </section>
        <section id="story-intro" class="story-intro">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="card mb-3">
                            <img class="card-img-top center mt-5"
                                style="width: 70%; display: block; margin-left: auto; margin-right: auto;"
                                src="{{ asset('fronten/assets/img/gallery/berita3.jpg') }}" alt="...">
                            <div class="card-body mt-5">
                                <h2 class="card-title">[Program Dakwah, YBM PLN Adakan Training Imam dan Khatib]</h2>
                                <p>
                                    Singkawang (11/04) - YBM PLN Kalimantan Barat kembali melaksanakan Training Imam dan
                                    Khatib Ke-82 yang dilaksanakan pada 9-13 April 2019 di Singkawang bekerjasama dengan
                                    BAZNAS Provinsi Kalbar, AMCF, Kemenag Provinsi Kalbar, FOZ Kalbar beserta Ormas-ormas
                                    Islam.
                                </p>
                                <p>
                                    Training yang dikhususkan bagi Da'i, guru ngaji dan Mualaf ini dihadiri 39 peserta
                                    dimana YBM PLN UIW Kalbar mengirimkan sebanyak 9 da'i pedalaman dan 6 orang tokoh mualaf
                                    yang berasal dr pelosok Kabupaten Sambas dan Kabupaten Bengkayang.
                                </p>
                                <p>
                                    Dengan adanya pelatihan ini diharapkan seluruh Da'i, guru ngaji dan tokoh Mualaf
                                    mempunyai kemampuan yang sama dalam hal melakukan aktivitas dakwah serta bisa
                                    meningkatkan agresivitas dalam dakwah untuk menangkal pemurtadan yang semakin massif di
                                    Kalimantan Barat, mengingat menurut hasil survey IRP yg dilaksanakan oleh BAZNAS tahun
                                    2018 Kalbar menempati urutan teratas tingkat pemurtadan di Indonesia.
                                </p>
                                <p>
                                    Da'i pedalaman YBM PLN yang hadir dalam training ini mengatakan bahwa banyak sekali
                                    muslim dan mualaf di daerah pedalaman mengharapkan adanya pembinaan dan kehadiran para
                                    Da'i untuk terjun dan membina langsung didaerah mereka. Oleh sebab itu, YBM PLN
                                    mengadakan kegiatan ini sehingga muslim di Kalimantan Barat mendapatkan pembinaan
                                    berkualitas agar lebih baik dalam agama dan ibadahnya.
                                </p>
                                <p style="color:blue;">
                                    #ybmpln #pln #menjejakmanfaat #dakwah #islam #training #mualaf #baznas
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

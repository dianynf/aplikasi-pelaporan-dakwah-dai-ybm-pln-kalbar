@extends('layouts.main')
@section('title', "Gambar - Da'i YBM PLN KAL-BAR")

@section('content')
    <main id="main">
        <section class="inner-page">
            <div class="container">
            </div>
        </section>
        <section id="gallery" class="gallery">
            <div class="container">

                <div class="row">
                    <div class="col-lg-12 d-flex justify-content-center">
                        <ul id="gallery-flters">
                            <li data-filter="*" class="filter-active">All</li>
                            <li data-filter=".filter-home">Dai</li>
                            <li data-filter=".filter-beach">Mualaf</li>
                            <li data-filter=".filter-vacation">Kegiatan</li>
                        </ul>
                    </div>
                </div>

                <div class="row gallery-container">

                    <div class="col-lg-4 col-md-6 gallery-item filter-home">
                        <div class="gallery-wrap">
                            <img src="{{ asset('fronten/assets/img/gallery/berita1.png') }}" class="img-fluid" alt="">
                            <div class="gallery-info">
                                <h4>Dai</h4>
                                <div class="gallery-links">
                                    <a href="{{ asset('fronten/assets/img/gallery/berita1.png') }}"
                                        data-gall="galleryGallery" class="venobox" title="Dai"><i
                                            class="bx bx-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 gallery-item filter-home">
                        <div class="gallery-wrap">
                            <img src="{{ asset('fronten/assets/img/gallery/berita2.png') }}" class="img-fluid" alt="">
                            <div class="gallery-info">
                                <h4>Dai</h4>
                                <div class="gallery-links">
                                    <a href="{{ asset('fronten/assets/img/gallery/berita2.png') }}"
                                        data-gall="galleryGallery" class="venobox" title="Dai"><i
                                            class="bx bx-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 gallery-item filter-vacation">
                        <div class="gallery-wrap">
                            <img src="{{ asset('fronten/assets/img/gallery/berita3.jpg') }}" class="img-fluid" alt="">
                            <div class="gallery-info">
                                <h4>Kegiatan</h4>
                                <div class="gallery-links">
                                    <a href="{{ asset('fronten/assets/img/gallery/berita3.jpg') }}"
                                        data-gall="galleryGallery" class="venobox" title="Kegiatan"><i
                                            class="bx bx-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 gallery-item filter-vacation">
                        <div class="gallery-wrap">
                            <img src="{{ asset('fronten/assets/img/gallery/berita4.png') }}" class="img-fluid" alt="">
                            <div class="gallery-info">
                                <h4>Kegiatan</h4>
                                <div class="gallery-links">
                                    <a href="{{ asset('fronten/assets/img/gallery/berita4.png') }}"
                                        data-gall="galleryGallery" class="venobox" title="Kegiatan"><i
                                            class="bx bx-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 gallery-item filter-vacation">
                        <div class="gallery-wrap">
                            <img src="{{ asset('fronten/assets/img/gallery/berita5.png') }}" class="img-fluid" alt="">
                            <div class="gallery-info">
                                <h4>Kegiatan</h4>
                                <div class="gallery-links">
                                    <a href="{{ asset('fronten/assets/img/gallery/berita5.png') }}"
                                        data-gall="galleryGallery" class="venobox" title="Kegiatan"><i
                                            class="bx bx-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 gallery-item filter-vacation">
                        <div class="gallery-wrap">
                            <img src="{{ asset('fronten/assets/img/gallery/berita7.png') }}" class="img-fluid" alt="">
                            <div class="gallery-info">
                                <h4>Kegiatan</h4>
                                <div class="gallery-links">
                                    <a href="{{ asset('fronten/assets/img/gallery/berita7.png') }}"
                                        data-gall="galleryGallery" class="venobox" title="Kegiatan"><i
                                            class="bx bx-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 gallery-item filter-vacation">
                        <div class="gallery-wrap">
                            <img src="{{ asset('fronten/assets/img/gallery/berita8.jpeg') }}" class="img-fluid" alt="">
                            <div class="gallery-info">
                                <h4>Kegiatan</h4>
                                <div class="gallery-links">
                                    <a href="{{ asset('fronten/assets/img/gallery/berita8.jpeg') }}"
                                        data-gall="galleryGallery" class="venobox" title="Kegiatan"><i
                                            class="bx bx-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 gallery-item filter-vacation">
                        <div class="gallery-wrap">
                            <img src="{{ asset('fronten/assets/img/gallery/berita9.jpeg') }}" class="img-fluid" alt="">
                            <div class="gallery-info">
                                <h4>Kegiatan</h4>
                                <div class="gallery-links">
                                    <a href="{{ asset('fronten/assets/img/gallery/berita9.jpeg') }}"
                                        data-gall="galleryGallery" class="venobox" title="Kegiatan"><i
                                            class="bx bx-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 gallery-item filter-vacation">
                        <div class="gallery-wrap">
                            <img src="{{ asset('fronten/assets/img/gallery/berita10.png') }}" class="img-fluid" alt="">
                            <div class="gallery-info">
                                <h4>Kegiatan</h4>
                                <div class="gallery-links">
                                    <a href="{{ asset('fronten/assets/img/gallery/berita10.png') }}"
                                        data-gall="galleryGallery" class="venobox" title="Kegiatan"><i
                                            class="bx bx-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 gallery-item filter-home">
                        <div class="gallery-wrap">
                            <img src="{{ asset('fronten/assets/img/gallery/berita11.png') }}" class="img-fluid" alt="">
                            <div class="gallery-info">
                                <h4>Dai</h4>
                                <div class="gallery-links">
                                    <a href="{{ asset('fronten/assets/img/gallery/berita11.png') }}"
                                        data-gall="galleryGallery" class="venobox" title="Dai"><i
                                            class="bx bx-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- End Gallery Section -->

    </main>
@endsection

@extends('layouts.main')
@section('title', "Berita - Da'i YBM PLN KAL-BAR")

@section('content')
    <main id="main">
        <section class="inner-page">
            <div class="container">
            </div>
        </section>
        <section id="event-list" class="event-list">
            <div class="container">

                <div class="row">
                    <div class="col-md-4 d-flex align-items-stretch">
                        <div class="card">
                            <div class="card-img">
                                <img src="{{ asset('fronten/assets/img/gallery/berita1.png') }}" alt="...">
                            </div>
                            <div class="card-body">
                                <h6 class="card-title">Ustadz Yanto Dai pedalaman YBM PLN UIW Kalimantran Barat</h6>
                                <div class="text-center">
                                    <a href="{{ url('/berita/ustyanto') }}" class="btn btn-sm btn-berita">Selengkapnya</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex align-items-stretch">
                        <div class="card">
                            <div class="card-img">
                                <img src="{{ asset('fronten/assets/img/gallery/berita2.png') }}" alt="...">
                            </div>
                            <div class="card-body">
                                <h6 class="card-title">Program Dakwah "Beting to Bening"</h6>
                                <div class="text-center">
                                    <a href="{{ url('/berita/beting-to-bening') }}"
                                        class="btn btn-sm btn-berita">Selengkapnya</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex align-items-stretch">
                        <div class="card">
                            <div class="card-img">
                                <img src="{{ asset('fronten/assets/img/gallery/berita3.jpg') }}" alt="...">
                            </div>
                            <div class="card-body">
                                <h6 class="card-title">Program Dakwah, YBM PLN Adakan Training Imam dan Khatib</h6>
                                <div class="text-center">
                                    <a href="{{ url('/berita/training-imam-dan-khatib') }}"
                                        class="btn btn-sm btn-berita">Selengkapnya</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@extends('layouts.main')
@section('title', "Profile - Da'i YBM PLN KAL-BAR")

@section('content')
    <main id="main">
        <section class="inner-page">
            <div class="container">
            </div>
        </section>
        <section id="story-intro" class="story-intro">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 order-1 order-lg-2">
                        <img src="{{ asset('fronten/assets/img/icons.png') }}" class="img-fluid" alt="">
                    </div>
                    <div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content">
                        <h3>Visi</h3>
                        <p class="font-italic">
                            - Menjadi lembaga pengelola ZIS (zakat, infak, shadaqoh) terdepan yang amanah, profesional
                            dan transparan di lingkungan PT PLN (Persero) dalam memberdayakan mustahik menjadi muzakki.
                        </p>
                        <h3>Misi</h3>
                        <p>
                            - Melaksanakan pengelolaan zakat infaq, shodaqoh dan wakaf secara amanah, profesional dan
                            transparan sesuai tuntunan syari’ah.
                        </p>
                        <p>
                            - Mengoptimalkan potensi zakat infaq, shodaqoh dan wakaf pegawai PT PLN (Persero) yang
                            beragama Islam atau muzakki lainnya.
                        </p>
                        <p>
                            - Memberikan informasi, pembelajaran, pemberdayaan dan pembinaan kepada mustahik dan
                            masyarakat luas.
                        </p>
                        <p>- Memberdayakan mustahik menjadi muzakki.</p>
                    </div>
                </div>

            </div>
        </section>
    </main>
@endsection

@extends('layouts.main')
@section('title', "Profile - Da'i YBM PLN KAL-BAR")

@section('content')
    <main id="main">
        <section class="inner-page">
            <div class="container">
            </div>
        </section>
        <section id="contact-us" class="contact-us">
            <div class="container">
                @if (session()->has('kontak'))
                    <div class="alert alert-info col-xl-12 alert-dismissible fade show" role="alert">
                        {{ session()->get('kontak') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <div class="row mt-5">
                    <div class="col-lg-4">
                        <div class="info">
                            <div class="address">
                                <i class="icofont-google-map"></i>
                                <h4>SEKRETARIAT YBM PLN KAL-BAR:</h4>
                                <p>Alamat: Jl. Adi Sucipto No.4, Sungai Raya, Kec. Sungai Raya, Kabupaten Kubu Raya,
                                    Kalimantan Barat 78234</p>
                            </div>
                            <div class="email">
                                <i class="icofont-envelope"></i>
                                <h4>Email:</h4>
                                <p>ybmpln@gmail.com</p>
                            </div>
                            <div class="phone">
                                <i class="icofont-phone"></i>
                                <h4>Nomor Telpon</h4>
                                <p>+62 853-9364-****</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 mt-5 mt-lg-0">
                        <form method="POST" action="/kontak/store">
                            {{ csrf_field() }}
                            <div class="form-row">
                                <div class="col-md-6 form-group">
                                    <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Anda" />
                                    <div class="validate"></div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <input type="email" required class="form-control" name="email" id="email"
                                        placeholder="Email Anda" data-rule="email" data-msg="Please enter a valid email" />
                                    <div class="validate"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="subjek" id="subjek" placeholder="Subject" />
                                <div class="validate"></div>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="deskripsi" rows="5"
                                    data-msg="Please write something for us" placeholder="Pesan"></textarea>
                                <div class="validate"></div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-learn-btn col-md-4">Kirim</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

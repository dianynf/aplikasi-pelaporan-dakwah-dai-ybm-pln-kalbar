@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="card border-left-primary shadow mb-4">
            <div class="card-header py-3">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h5 class="h3 mb-0 text-gray-800">Edit Pesan</h5>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('pesan.update', $kontak->id) }}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="form-group">
                        <input type="text" class="form-control" id="email" name="email" required autocomplete="email"
                            placeholder="Email" autofocus value="{{ isset($kontak) ? $kontak->email : old('email') }}">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="nama" name="nama" autocomplete="nama" placeholder="Nama"
                            autofocus value="{{ isset($kontak) ? $kontak->nama : old('nama') }}">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="subjek" name="subjek" autocomplete="subjek"
                            placeholder="Subjek" autofocus value="{{ isset($kontak) ? $kontak->subjek : old('subjek') }}">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="deskripsi" name="deskripsi" required
                            autocomplete="deskripsi" placeholder="Deskripsi" autofocus
                            value="{{ isset($kontak) ? $kontak->deskripsi : old('deskripsi') }}">
                    </div>
                    <button type=" submit" class="btn btn-primary btn-block">Simpan</button>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection

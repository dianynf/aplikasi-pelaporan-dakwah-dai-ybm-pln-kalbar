@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="card border-left-primary shadow mb-4">
            @if (session()->has('sukses'))
                <div class="alert alert-info col-xl-12 alert-dismissible fade show" role="alert">
                    {{ session()->get('sukses') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card-header py-3">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h5 class="m-0 font-weight-bold text-gray-800 mt-2">Data Dai</h5>
                        <a href="{{ route('dai.create_dai') }}" class="btn btn-sm btn-primary mt-2"
                            style="color: #fff; margin-left: auto;"><i class="fas fa-plus"></i>
                            &nbsp;Tambah Dai</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Email</th>
                                <th scope="col">Role</th>
                                <th scope="col">Foto</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @foreach ($items as $item)
                                <tr>
                                    <th scope="row">{{ $no++ }}</th>
                                    <td>{{ $item->username }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>
                                        @if ($item->role_id == 1)
                                            Admin
                                        @else
                                            Da'i
                                        @endif
                                    </td>
                                    <td><img src="{{ Storage::url('public/fotoselfi/' . $item->image) }}"
                                            class="img-circle" alt="..." width="40" height="40"></td>
                                    <td>
                                        <a href="{{ route('dai.detail', $item->id) }}" class="btn btn-sm btn-primary">
                                            <i class="fa fa-search-plus"></i>
                                        </a>
                                        @if (Auth::user()->role_id == 1)
                                            <a href="{{ route('dai.ubah', $item->id) }}" class="btn btn-sm btn-info">
                                                <i class="fa fa-pencil-alt"></i>
                                            </a>
                                            {{-- <form action="{{ route('dai.delete', $item->id) }}" method="post"
                                                class="d-inline">
                                                @csrf
                                                @method('delete')
                                                <button class="btn btn-sm btn-danger">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </form> --}}
                                            <form method="POST" action="{{ route('dai.delete', $item->id) }}" class="d-inline">
                                                @csrf
                                                @method('delete')
                                                <input name="_method" type="hidden" value="DELETE">
                                                <button type="submit" class="btn btn-sm btn-danger btn-flat show_confirm" data-toggle="tooltip" title='Delete'><i class="fa fa-trash"></i></button>
                                            </form>
                                        @else
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection

@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="card border-left-primary shadow mb-4">
            <div class="card-header py-3">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h5 class="h3 mb-0 text-gray-800">Edit User {{ $user->name }}</h5>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('dai.simpan', $user->id) }}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="form-group">
                        <label class="label-color">Nama</label>
                        <input id="username" readonly type="text"
                            class="form-control @error('username') is-invalid @enderror" placeholder="Nama Lengkap" name="#"
                            value="{{ $user->username }}" required autofocus>
                        @error('username')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="label-color">Email</label>
                        <input id="email" readonly type="email" class="form-control" placeholder="E-Mail" name="#"
                            value="{{ $user->email }}" required autocomplete="email" autofocus>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="label-color">Role</label>
                        <select class="form-control" name="role_id" id="role_id">
                            <option class="hidden" selected disabled>Pilih Role</option>
                            @foreach ($roles as $item)
                                <option value="{{ $item->id }}" {{ $item->id == $user->role_id ? 'selected' : '' }}>
                                    {{ $item->name }} </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="label-color">Alamat</label>
                        <input id="alamat" readonly type="text" class="form-control" placeholder="Alamat" name="#"
                            value="{{ $user->alamat }}" required autofocus>
                        @error('alamat')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-4">
                                    <img src="{{ Storage::url('public/fotoselfi/' . $user->image) }}"
                                        class="img-thumbnail">
                                </div>
                                <div class="col-sm-8">
                                    <div class="custom-file">
                                        <input class="disabled" type="file" class="custom-file-input" name="#"
                                            placeholder="Image">
                                        <label class="custom-file-label" for="image">{{ $user->image }}</label>
                                    </div>
                                    <span>
                                        Gambar Photo Buku Tabungan Anda sebaiknya memiliki
                                        berukuran tidak lebih dari 2MB.
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type=" submit" class="btn btn-primary btn-block">Simpan</button>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection

@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="card border-left-primary shadow mb-4">
            <div class="card-header py-3">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h5 class="h3 mb-0 text-gray-800">Edit User {{ $role->name }}</h5>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ isset($role) ? route('role.update', $role->id) : route('role.store') }}" method="POST">
                    @csrf
                    @if (isset($role))
                        @method('PATCH')
                    @endif
                    <div class="form-group">
                        <input type="text" class="form-control" id="name" name="name" required autocomplete="name"
                            placeholder="Role" autofocus value="{{ isset($role) ? $role->name : old('name') }}">
                    </div>
                    <button type=" submit" class="btn btn-primary btn-block">Simpan</button>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection

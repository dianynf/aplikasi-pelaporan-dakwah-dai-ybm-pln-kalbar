@extends('layouts.admin')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Content Row -->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="col-xl-12 col-md-8 mb-5">
            @if (session()->has('sukses'))
                <div class="alert alert-info col-xl-12 alert-dismissible fade show" role="alert">
                    {{ session()->get('sukses') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="col-md-12">
            </div>
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="modal-body">
                            <h3 class="h3 mb-4 text-gray-800">Profile</h3>
                            <hr>
                            <form method="POST" action="{{ route('profile.update') }}" enctype="multipart/form-data">
                                @method('patch')
                                @csrf
                                <div class="form-group row">
                                    <label for="username" class="col-sm-2 col-form-label">Nama Lengkap</label>
                                    <div class="col-sm-10">
                                        <input id="username" type="text"
                                            class="form-control @error('username') is-invalid @enderror" name="username"
                                            value="{{ old('username', $users->username) }}" autocomplete="username"
                                            autofocus>
                                        @error('username')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input id="email" type="email"
                                            class="form-control @error('email') is-invalid @enderror" name="email"
                                            value="{{ old('email', $users->email) }}" autocomplete="email" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nohp" class="col-sm-2 col-form-label">Nomor Handphone</label>
                                    <div class="col-sm-10">
                                        <input id="nohp" type="number"
                                            class="form-control @error('nohp') is-invalid @enderror" name="nohp"
                                            value="{{ old('nohp', $users->nohp) }}" autocomplete="nohp">
                                        @error('nohp')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                        <input id="alamat" type="alamat"
                                            class="form-control @error('alamat') is-invalid @enderror" name="alamat"
                                            value="{{ old('alamat', $users->alamat) }}" autocomplete="alamat">
                                        @error('alamat')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="alamat" class="col-sm-2 col-form-label">Foto</label>
                                    <div class="col-sm-10">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <img src="{{ Storage::url('public/fotoselfi/' . $users->image) }}"
                                                    class="img-thumbnail">
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="image" name="image"
                                                        placeholder="Image">
                                                    <label class="custom-file-label"
                                                        for="image">{{ $users->image }}</label>
                                                </div>
                                                <span>
                                                    Gambar Photo Buku Tabungan Anda sebaiknya memiliki
                                                    berukuran tidak lebih dari 2MB.
                                                </span>
                                            </div>
                                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
                                            </script>
                                            <script>
                                                // Add the following code if you want the name of the file appear on select
                                                $(".custom-file-input").on("change", function() {
                                                    var fileName = $(this).val().split("\\").pop();
                                                    $(this).siblings(".custom-file-label").addClass("selected")
                                                        .html(fileName);
                                                });

                                            </script>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection
